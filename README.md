# XO #
*"Strange game. The only winning move is not to play." - Joshua, WarGames*

A simple [tic-tac-toe game](https://en.wikipedia.org/wiki/Tic-tac-toe) implementation.

### What is this repository for? ###

This is a simple demo project.

### How do I launch it? ###

* download [`build/xo-1.0.zip`](https://bitbucket.org/seaw0lf/xo/raw/73e0a34e09c58b5ee9538b8742f4bc32c9d04b75/build/xo-1.0.zip)
* unzip
* run `xo-computerBegins` or `xo-playerBegins` on JRE 8

### What is `xo` for? ###

`xo` is the game launcher which takes three arguments (automaton, player X implementation, and player O implementation). Currently, there are 6 player implementations:

* `xo.player.impl.RandomAutonomousPlayer` - computer/random moves
* `xo.player.impl.MinimaxAutonomousPlayer` - computer/AI
* `xo.player.impl.ConsoleInteractivePlayer` - human/console user interface
* `xo.player.impl.SocketInteractivePlayer` - human/telnet
* `xo.player.impl.SwingInteractivePlayer` - human/GUI
* `xo.player.impl.FXInteractivePlayer` - human/GUI

Thus, it is possible to launch the game with various player combinations:

* human/GUI vs. human/telnet:
  `xo xo.competition.Competition xo.player.impl.FXInteractivePlayer xo.player.impl.SocketInteractivePlayer`
* computer/random vs. computer/AI:
  `xo xo.competition.Competition xo.player.impl.RandomAutonomousPlayer xo.player.impl.MinimaxAutonomousPlayer`
* computer/AI vs. computer/AI:
  `xo xo.competition.Competition xo.player.impl.MinimaxAutonomousPlayer xo.player.impl.MinimaxAutonomousPlayer`
* ...

### What is `xo.competition.Competition`? ###

`xo.competition.Competition` is an automaton representing tic-tac-toe *game*, i.e. *match*, score update, *match*, score update, ...

`xo.match.Match` is another automaton representing one tic-tac-toe *match*.

It is possible to launch only one match, e.g.

* `xo xo.match.Match xo.player.impl.ConsoleInteractivePlayer xo.player.impl.RandomAutonomousPlayer`
* ...

### Why are there two `.jar`s? ###

`xo-core.jar` contains:

* automaton abstraction
* competition & match implementation
* lightweight player implementation

`xo-player.jar` contains:

* [minimax](https://en.wikipedia.org/wiki/Minimax) autonomous player implementation
* Swing/GUI implementation
* JavaFX/GUI implementation

### What about logging? ###

`java.util.logging` facility can be configured via `conf/logging.properties` and stores logs into `logs/` directory.

### Documentation ###

* `doc/javadoc`
* [`doc/uml`](https://bitbucket.org/seaw0lf/xo/src/f0cec244f1f5f8c5e8d54dcd21d8272dc2df1239/doc/uml/?at=master)

### Tools ###

* Java 8 (JDK 1.8.0_66)
* Eclipse Mars
* JavaFX Scene Builder 8.0.0
* JUnit 4 & Hamcrest 1.3
* ObjectAid
* Git