package automaton;

import org.hamcrest.CoreMatchers;
import org.junit.*;

import automaton.finite.*;
import xo.impl.*;

public class FiniteStateAutomatonTest {

  /**
   * Test method for {@link StateAutomaton#changeState()} with limit.
   */
  @Test(expected = TransitionLimitExceededException.class, timeout = 100)
  public void terminateOnDefaultLimit() throws Exception {

    try (FiniteStateAutomaton<TestState, TestSymbol> a = new FiniteStateAutomaton<TestState, TestSymbol>() {

      @Override
      protected void open() {
      }

      @Override
      protected boolean finalState() {
        return false;
      }

      @Override
      public void close() {
      }
    }) {
      a.start();
    }
  }

  /**
   * Test method for {@link StateAutomaton#changeState()} with limit.
   */
  @Test(timeout = 100)
  public void terminateOnSpecificLimit() throws Exception {
    final int iterations = 10;

    try (FiniteStateAutomaton<TestState, TestSymbol> a = new FiniteStateAutomaton<TestState, TestSymbol>() {
      private final TestState.Factory factory = new TestState.Factory();
      private int nextStateCounter = 0;

      @Override
      protected void open() {
        state = factory.startState();
        transition = factory;
        transitions(iterations);
        inputSupplier = state -> TestSymbol.REGULAR;
      }

      @Override
      public void start() {
        try {
          super.start();
        } catch (Throwable t) {
          Assert.assertThat(t, CoreMatchers.instanceOf(TransitionLimitExceededException.class));
          Assert.assertEquals("Incorrect nextState invocation count", iterations, nextStateCounter);
        }
      }

      @Override
      protected boolean finalState() {
        return false;
      }

      @Override
      protected TestState nextState() throws UnsuccessfulStateChangeException, TerminatedStateAutomatonException {
        nextStateCounter++;
        return super.nextState();
      }

      @Override
      public void close() {
      }
    }) {
      a.start();
    }
  }
}
