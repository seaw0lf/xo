package automaton;

import java.util.*;
import java.util.function.Consumer;

import org.junit.Test;

import xo.impl.*;

public class ObservableTest {

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(timeout = 500)
  public void noListener() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.FINAL)) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(new EnumMap<>(TestStateAutomatonListener.Event.class));
      try {
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}, {@link Observable#removeListener(EventListener)} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(timeout = 500)
  public void removedListener() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.FINAL)) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(new EnumMap<>(TestStateAutomatonListener.Event.class));
      try {
        a.addListener(listener);
        a.removeListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}, {@link Observable#removeListeners()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(timeout = 500)
  public void removedAllListener() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.FINAL)) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(new EnumMap<>(TestStateAutomatonListener.Event.class));
      try {
        a.addListener(listener);
        a.removeListeners();
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(timeout = 500)
  public void startStopEvents() throws Exception {

    Map<TestStateAutomatonListener.Event, Integer> order = new EnumMap<>(TestStateAutomatonListener.Event.class);
    order.put(TestStateAutomatonListener.Event.automatonStarted, 1);
    order.put(TestStateAutomatonListener.Event.beforeStateChange, 2);
    order.put(TestStateAutomatonListener.Event.afterStateChange, 3);
    order.put(TestStateAutomatonListener.Event.automatonStopped, 4);

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.FINAL)) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(order);
      try {
        a.addListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(timeout = 500)
  public void unsuccessfulStateChangeEvent() throws Exception {

    Map<TestStateAutomatonListener.Event, Integer> order = new EnumMap<>(TestStateAutomatonListener.Event.class);
    order.put(TestStateAutomatonListener.Event.automatonStarted, 1);
    order.put(TestStateAutomatonListener.Event.beforeStateChange, 2);
    order.put(TestStateAutomatonListener.Event.unsuccessfulStateChange, 3);
    order.put(TestStateAutomatonListener.Event.automatonStopped, 4);

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.ERROR) {
      private boolean finalState = false;

      @Override
      protected boolean finalState() {
        try {
          return finalState;
        } finally {
          finalState = true;
        }
      }
    }) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(order);
      try {
        a.addListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(expected = TerminatedStateAutomatonException.class, timeout = 500)
  public void terminateEvent() throws Exception {

    Map<TestStateAutomatonListener.Event, Integer> order = new EnumMap<>(TestStateAutomatonListener.Event.class);
    order.put(TestStateAutomatonListener.Event.automatonStarted, 1);
    order.put(TestStateAutomatonListener.Event.beforeStateChange, 2);
    order.put(TestStateAutomatonListener.Event.automatonTerminated, 3);

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.TERMINATOR) {
      private boolean finalState = false;

      @Override
      protected boolean finalState() {
        try {
          return finalState;
        } finally {
          finalState = true;
        }
      }
    }) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(order);
      try {
        a.addListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(expected = IllegalArgumentException.class, timeout = 500)
  public void exceptionalInputEvent() throws Exception {

    Map<TestStateAutomatonListener.Event, Integer> order = new EnumMap<>(TestStateAutomatonListener.Event.class);
    order.put(TestStateAutomatonListener.Event.automatonStarted, 1);
    order.put(TestStateAutomatonListener.Event.beforeStateChange, 2);
    order.put(TestStateAutomatonListener.Event.unsuccessfulStateChange, 3);
    order.put(TestStateAutomatonListener.Event.automatonTerminated, 4);

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> {
      throw new IllegalArgumentException();
    })) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(order);
      try {
        a.addListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()} and {@link Observable#notifyListeners(Consumer)}.
   */
  @Test(expected = IllegalArgumentException.class, timeout = 500)
  public void exceptionalTransitionEvent() throws Exception {

    Map<TestStateAutomatonListener.Event, Integer> order = new EnumMap<>(TestStateAutomatonListener.Event.class);
    order.put(TestStateAutomatonListener.Event.automatonStarted, 1);
    order.put(TestStateAutomatonListener.Event.beforeStateChange, 2);
    order.put(TestStateAutomatonListener.Event.unsuccessfulStateChange, 3);
    order.put(TestStateAutomatonListener.Event.automatonTerminated, 4);

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.REGULAR) {
      @Override
      protected void open() {
        super.open();
        transition = (state, symbol) -> {
          throw new IllegalArgumentException();
        };
      }
    }) {
      TestStateAutomatonListener listener = new TestStateAutomatonListener(order);
      try {
        a.addListener(listener);
        a.start();
      } finally {
        listener.close();
      }
    }
  }
}
