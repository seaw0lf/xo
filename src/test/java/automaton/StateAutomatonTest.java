package automaton;

import org.junit.Assert;
import org.junit.Test;

import xo.impl.*;

public class StateAutomatonTest {

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(timeout = 100)
  public void executionOrderOnFinalStartState() throws Exception {

    // An automaton that does nothing since it starts in final state.
    try (StateAutomaton<TestState, TestSymbol> a = new StateAutomaton<TestState, TestSymbol>() {
      private int counter = 0;

      @Override
      protected void open() {
        counter++;
        Assert.assertEquals("Initialization must be executed first", 1, counter);
      }

      @Override
      protected void run() throws TerminatedStateAutomatonException {
        counter++;
        Assert.assertEquals("Run must be executed after initialization", 2, counter);
        super.run();
      }

      @Override
      protected boolean finalState() {
        counter++;
        Assert.assertEquals("Final state check must preceed state change", 3, counter);
        return true;
      }

      @Override
      protected void changeState() throws TerminatedStateAutomatonException {
        Assert.fail("Start state is final; change state must not execute");
      }

      @Override
      public void close() {
        counter++;
        Assert.assertEquals("Close must be the last", 4, counter);
      }
    }) {
      a.start();
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(timeout = 100)
  public void executionOrderOnNonFinalStartState() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new StateAutomaton<TestState, TestSymbol>() {
      private static final int iterTotal = 10;
      private int iter = -1;
      private int counter = 0;

      @Override
      protected void open() {
        counter++;
        Assert.assertEquals("Initialization must be executed first", 1, counter);

        inputSupplier = state -> {
          counter++;
          Assert.assertEquals("Supply input symbol must must be called after next state", 6 + 5 * iter, counter);
          return null;
        };

        transition = (state, symbol) -> {
          counter++;
          Assert.assertEquals("Change state using function must must be called after retrieving input symbol", 7 + 5 * iter, counter);
          return state;
        };
      }

      @Override
      protected void run() throws TerminatedStateAutomatonException {
        counter++;
        Assert.assertEquals("Run must be executed after initialization", 2, counter);
        super.run();
      }

      @Override
      protected boolean finalState() {
        counter++;
        iter++;
        Assert.assertEquals("Final state check must be called after run/change state using function", 3 + 5 * iter, counter);
        return iter > iterTotal;
      }

      @Override
      protected void changeState() throws TerminatedStateAutomatonException {
        counter++;
        Assert.assertEquals("Change state must must be called after final state", 4 + 5 * iter, counter);
        super.changeState();
      }

      @Override
      protected TestState nextState() throws UnsuccessfulStateChangeException, TerminatedStateAutomatonException {
        counter++;
        Assert.assertEquals("Next state must must be called after change state", 5 + 5 * iter, counter);
        return super.nextState();
      }

      @Override
      public void close() {
        counter++;
        Assert.assertEquals("Close must be the last", 3 + 5 * iter + 1, counter);
      }
    }) {
      a.start();
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(expected = TerminatedStateAutomatonException.class, timeout = 100)
  public void terminateOnStartState() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.TERMINATOR)) {
      a.start();
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(expected = TerminatedStateAutomatonException.class, timeout = 100)
  public void terminateOnNonStartState() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(new TestSupplier(TestSymbol.REGULAR, TestSymbol.REGULAR, TestSymbol.TERMINATOR))) {
      a.start();
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(timeout = 200)
  public void unsuccessfulStateChange() throws Exception {

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(state -> TestSymbol.ERROR) {
      private boolean finalState = false;

      @Override
      protected boolean finalState() {
        try {
          return finalState;
        } finally {
          finalState = true;
        }
      }
    }) {
      a.start();
      Assert.assertEquals("Start state expected", 0, a.getState().getIndex());
    }
  }

  /**
   * Test method for {@link StateAutomaton#start()}.
   */
  @Test(timeout = 100)
  public void run() throws Exception {

    TestSymbol[] input = { TestSymbol.ERROR, TestSymbol.REGULAR, TestSymbol.ERROR, TestSymbol.REGULAR, TestSymbol.ERROR, TestSymbol.REGULAR, TestSymbol.ERROR, TestSymbol.REGULAR, TestSymbol.ERROR, TestSymbol.FINAL };

    try (StateAutomaton<TestState, TestSymbol> a = new TestStateAutomaton(new TestSupplier(input))) {
      a.start();
      Assert.assertEquals("Incorrect state", input.length / 2 /* 50% of the input is invalid */, a.getState().getIndex());
    }
  }
}
