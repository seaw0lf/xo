package xo.competition;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

import java.util.*;

import org.junit.*;

import xo.match.*;
import xo.match.Grid.Position;
import xo.player.*;

public class PlayersTest {

  /**
   * Player whose instantiation always fails.
   */
  public static class FailingPlayer implements Player {

    @Override
    public void init(Tag tag) throws Exception {
      throw new UnsupportedOperationException();
    }

    @Override
    public Position apply(Board board) {
      return null;
    }

    @Override
    public void close() throws Exception {
    }
  }

  /**
   * Player whose instantiation always succeeds.
   */
  public static class PassingPlayer implements Player {
    private Tag tag;
    private boolean closed;

    @Override
    public void init(Tag tag) throws Exception {
      Player.super.init(this.tag = tag);
    }

    @Override
    public Position apply(Board board) {
      return null;
    }

    @Override
    public void close() throws Exception {
      closed = true;
    }
  }

  /**
   * Test method for {@link Players#get(Tag)} with {@code null} Tag.
   */
  @Test(expected = NullPointerException.class)
  public void testGetNullTag() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {
      players.get(null);
    }
  }

  /**
   * Test method for {@link Players#get(Tag)}.
   */
  @Test
  public void testGet() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    Player player;

    try (Players players = Players.forName(playerClassName)) {

      player = players.get(Tag.X);
      Assert.assertTrue(player instanceof PassingPlayer);
      Assert.assertEquals(Tag.X, ((PassingPlayer) player).tag);

      player = players.get(Tag.O);
      Assert.assertTrue(player instanceof PassingPlayer);
      Assert.assertEquals(Tag.O, ((PassingPlayer) player).tag);
    }
  }

  /**
   * Test method for {@link Players#iterator()}.
   */
  @Test
  public void testIterator() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {

      for (Player player : players) {
        Assert.assertTrue(player instanceof PassingPlayer);
        Assert.assertThat(((PassingPlayer) player).tag, anyOf(is(Tag.X), is(Tag.O)));
      }
    }
  }

  /**
   * Test method for {@link Players#close()}.
   */
  @Test
  public void testClose() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    Players players = Players.forName(playerClassName);
    players.close();

    for (Player player : players) {
      Assert.assertTrue(player instanceof PassingPlayer);
      Assert.assertTrue(((PassingPlayer) player).closed);
    }
  }

  /**
   * Test method for {@link Players#forName(Map)} with {@code null} Map.
   */
  @Test(expected = IllegalStateException.class)
  public void testRegisterNullPlayers() {
    try (Players players = Players.forName(null)) {
    }
  }

  /**
   * Test method for {@link Players#forName(Map)} with empty Map.
   */
  @Test(expected = IllegalStateException.class)
  public void testRegisterNoPlayer() {
    try (Players players = Players.forName(new EnumMap<>(Tag.class))) {
    }
  }

  /**
   * Test method for {@link Players#forName(Map)}.
   */
  @Test(expected = IllegalStateException.class)
  public void testRegisterPassingPlayer() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {
    }
  }

  /**
   * Test method for {@link Players#forName(Map)}.
   */
  @Test(expected = IllegalStateException.class)
  public void testRegisterFailingPlayers() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, FailingPlayer.class.getName());
    playerClassName.put(Tag.O, FailingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {
    }
  }

  /**
   * Test method for {@link Players#forName(Map)}.
   */
  @Test(expected = IllegalStateException.class)
  public void testRegisterFailingPlayer() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, FailingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {
    }
  }

  /**
   * Test method for {@link Players#forName(Map)}.
   */
  @Test
  public void testRegisterPassingPlayers() {

    Map<Tag, String> playerClassName = new EnumMap<>(Tag.class);
    playerClassName.put(Tag.X, PassingPlayer.class.getName());
    playerClassName.put(Tag.O, PassingPlayer.class.getName());

    try (Players players = Players.forName(playerClassName)) {
    }
  }
}
