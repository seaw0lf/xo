package xo.competition;

import java.util.*;

import org.junit.*;

import xo.player.Tag;

public class ScoreTest {
  private static Random RANDOM = new Random();

  /**
   * Test method for {@link Score#getScore(Tag)} with {@code null} Tag.
   */
  @Test(expected = NullPointerException.class)
  public void testScoreNullTag() {
    new Score.Factory(Tag.X).startState().getScore(null);
  }

  /**
   * Test method for {@link Score#getScore(Tag)}.
   */
  @Test
  public void testScore() {

    Score.Factory factory = new Score.Factory(Tag.X);
    Score score = factory.startState();

    Tag[] tags = Tag.values();

    Map<Tag, Integer> scoreExpected = new EnumMap<>(Tag.class);
    for (Tag tag : tags) {
      scoreExpected.put(tag, 0);
    }

    for (int i = 0; i < 1024; i++) {

      Assert.assertEquals(scoreExpected.get(Tag.X).intValue(), score.getScore(Tag.X));
      Assert.assertEquals(scoreExpected.get(Tag.O).intValue(), score.getScore(Tag.O));

      Tag tag;
      try {
        tag = tags[RANDOM.nextInt(tags.length + 1)];
        scoreExpected.put(tag, scoreExpected.get(tag) + 1);
      } catch (ArrayIndexOutOfBoundsException e) {
        // Index out of bounds is interpreted as null tag
        tag = null;
      }
      score = factory.apply(score, tag);
    }
  }

  /**
   * Test method for {@link Score#getOnTurn()}.
   */
  @Test
  public void testOnTurn() {

    Score.Factory factory;
    Score score;

    {
      factory = new Score.Factory(Tag.X);
      score = factory.startState();
      Assert.assertEquals(Tag.X, score.getOnTurn());

      // 3x draw
      score = factory.apply(score, null);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, null);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, null);
      Assert.assertEquals(Tag.O, score.getOnTurn());

      // 3x X wins
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());

      // 3x O wins
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());

      // 2x interleaved X/O win
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());

      // 2x interleaved X/O win and draw
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, null);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, null);
      Assert.assertEquals(Tag.X, score.getOnTurn());
    }

    {
      factory = new Score.Factory(Tag.O);

      score = factory.startState();
      Assert.assertEquals(Tag.O, score.getOnTurn());

      score = factory.apply(score, null);
      Assert.assertEquals(Tag.X, score.getOnTurn());
      score = factory.apply(score, Tag.X);
      Assert.assertEquals(Tag.O, score.getOnTurn());
      score = factory.apply(score, Tag.O);
      Assert.assertEquals(Tag.X, score.getOnTurn());
    }
  }

  /**
   * Test method for {@link Score#isFinal()}.
   */
  @Test
  public void testIsFinal() {

    Score.Factory factory = new Score.Factory(Tag.X);
    Score score = factory.startState();

    Tag[] tags = Tag.values();

    for (int i = 0; i < 1024; i++) {
      Assert.assertFalse(score.isFinal());

      Tag tag;
      try {
        tag = tags[RANDOM.nextInt(tags.length + 1)];
      } catch (ArrayIndexOutOfBoundsException e) {
        // Index out of bounds is interpreted as null tag
        tag = null;
      }
      score = factory.apply(score, tag);
    }
  }

  /**
   * Test method for {@link Score.Factory#Factory(Tag)} with {@code null} Tag.
   */
  @Test(expected = NullPointerException.class)
  public void testFactoryNullTag() {
    new Score.Factory(null);
  }

  /**
   * Test method for {@link Score.Factory#apply(Score, Tag)} with {@code null} Score.
   */
  @Test(expected = NullPointerException.class)
  public void testFactoryApplyNullScore() {
    new Score.Factory(Tag.X).apply(null, Tag.O);
  }

  /**
   * Test method for {@link Score.Factory#apply(Score, Tag)} with {@code null} Tag.
   */
  @Test
  public void testFactoryApplyNullTag() {

    Score.Factory factory = new Score.Factory(Tag.X);

    factory.apply(factory.startState(), null);
  }
}
