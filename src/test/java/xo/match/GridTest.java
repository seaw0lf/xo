package xo.match;

import org.junit.*;

import xo.match.Grid.Position;
import xo.player.Tag;

public class GridTest {

  /**
   * Test method for {@link Grid#emptyPositions()}.
   */
  @Test
  public void testEmptyPositions() {
    Assert.assertEquals(9, parse("___" + "___" + "___").emptyPositions());
    Assert.assertEquals(8, parse("___" + "___" + "X__").emptyPositions());
    Assert.assertEquals(7, parse("__X" + "___" + "X__").emptyPositions());
    Assert.assertEquals(6, parse("__X" + "__O" + "X__").emptyPositions());
    Assert.assertEquals(5, parse("X_X" + "__O" + "X__").emptyPositions());
    Assert.assertEquals(4, parse("X_X" + "__O" + "X_X").emptyPositions());
    Assert.assertEquals(3, parse("X_X" + "O_O" + "X_X").emptyPositions());
    Assert.assertEquals(2, parse("XOX" + "O_O" + "X_X").emptyPositions());
    Assert.assertEquals(1, parse("XOX" + "O_O" + "XOX").emptyPositions());
    Assert.assertEquals(0, parse("XOX" + "OXO" + "XOX").emptyPositions());
  }

  /**
   * Test method for {@link Grid#tag(Position)} with {@code null} Position.
   */
  @Test(expected = NullPointerException.class)
  public void testTagNull() {
    new Grid().tag(null);
  }

  /**
   * Test method for {@link Grid#tag(Position)}.
   */
  @Test
  public void testTagEmptyGrid() {

    Grid grid = new Grid();

    for (Position position : Position.values()) {
      Assert.assertEquals(null, grid.tag(position));
    }
  }

  /**
   * Test method for {@link Grid#tag(Position)}.
   */
  @Test
  public void testTagNonEmptyGrid() {

    Grid grid = parse("XOX" + "O_O" + "X_X");

    Assert.assertEquals(Tag.X, grid.tag(Position.NW));
    Assert.assertEquals(Tag.O, grid.tag(Position.N));
    Assert.assertEquals(Tag.X, grid.tag(Position.NE));
    Assert.assertEquals(Tag.O, grid.tag(Position.W));
    Assert.assertEquals(null, grid.tag(Position.C));
    Assert.assertEquals(Tag.O, grid.tag(Position.E));
    Assert.assertEquals(Tag.X, grid.tag(Position.SW));
    Assert.assertEquals(null, grid.tag(Position.S));
    Assert.assertEquals(Tag.X, grid.tag(Position.SE));
  }

  /**
   * Test method for {@link Grid#tag(Position, Tag)} with {@code null} Position.
   */
  @Test(expected = NullPointerException.class)
  public void testSetTagNullPosition() {
    new Grid().tag(null, Tag.X);
  }

  /**
   * Test method for {@link Grid#tag(Position, Tag)} with {@code null} Tag.
   */
  @Test(expected = NullPointerException.class)
  public void testSetTagNullTag() {
    new Grid().tag(Position.C, null);
  }

  /**
   * Test method for {@link Grid#tag(Position, Tag)}.
   */
  @Test
  public void testSetTagImmutable() {

    Grid grid = new Grid();
    Grid gridNew = grid.tag(Position.C, Tag.X);

    Assert.assertNotSame(grid, gridNew);

    Assert.assertEquals(null, grid.tag(Position.C));
    Assert.assertEquals(Tag.X, gridNew.tag(Position.C));
  }

  /**
   * Test method for {@link Grid#allInRow(Position[])} with {@code null} Position[].
   */
  @Test(expected = NullPointerException.class)
  public void testAllInRowNullRow() {
    parse("___" + "___" + "___").allInRow(null);
  }

  /**
   * Test method for {@link Grid#allInRow(Position[])}.
   */
  @Test
  public void testAllInRow() {

    Assert.assertEquals(null, parse("___" + "___" + "___").allInRow(Position.ROWS.get(7)));

    Assert.assertEquals(null, parse("___" + "___" + "OXO").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("___" + "OOO" + "___").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("XXX" + "___" + "___").allInRow(Position.ROWS.get(7)));

    Assert.assertEquals(null, parse("X__" + "_X_" + "__X").allInRow(Position.ROWS.get(7)));

    Assert.assertEquals(null, parse("___" + "_X_" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__X" + "___" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__X" + "_X_" + "___").allInRow(Position.ROWS.get(7)));

    Assert.assertEquals(Tag.X, parse("__X" + "_X_" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__O" + "_X_" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__X" + "_O_" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__X" + "_X_" + "O__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__O" + "_X_" + "O__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__X" + "_O_" + "O__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(null, parse("__O" + "_O_" + "X__").allInRow(Position.ROWS.get(7)));
    Assert.assertEquals(Tag.O, parse("__O" + "_O_" + "O__").allInRow(Position.ROWS.get(7)));
  }

  /**
   * Parse grid from a String.
   * @param grid String representation of a grid
   * @return Grid derived from a String
   */
  public static Grid parse(String grid) {

    Grid result = new Grid();

    for (Position p : Position.values()) {
      try {
        result = result.tag(p, Tag.valueOf(String.valueOf(grid.charAt(index(p)))));
      } catch (IllegalArgumentException e) {
        // 'No enum constant' ignored
      }
    }

    return result;
  }

  /**
   * Position to String index mapping.
   * @param position Position
   * @return String index corresponding to the {@link Position position}
   */
  static int index(Position position) {
    switch (position) {
      case NW: return 0;
      case N: return 1;
      case NE: return 2;
      case W: return 3;
      case C: return 4;
      case E: return 5;
      case SW: return 6;
      case S: return 7;
      case SE: return 8;
      default:
        throw new IllegalArgumentException("Position: " + position);
    }
  }
}
