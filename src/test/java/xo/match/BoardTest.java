package xo.match;

import java.util.*;

import org.junit.*;

import xo.match.Board.Status;
import xo.match.Grid.Position;
import xo.player.Tag;

public class BoardTest {

  /**
   * Test method for {@link Board#emptyPositions()}.
   */
  @Test
  public void testEmpty() {

    Board.Factory factory = new Board.Factory(Tag.X);
    Board board = factory.startState();

    LinkedList<Position> positions = new LinkedList<>(Arrays.asList(parse(Tag.X, "XOX" + "O_O" + "XOX")));
    positions.add(Position.C);

    while (!positions.isEmpty()) {
      Assert.assertEquals(positions.size(), board.emptyPositions());
      board = factory.apply(board, positions.removeFirst());
    }
    Assert.assertEquals(positions.size(), board.emptyPositions());
  }

  /**
   * Test method for {@link Board#isFinal()}.
   */
  @Test
  public void testIsFinal() {
    String grid;

    grid = "___" + "___" + "___";
    Assert.assertEquals("Empty grid", false, board(Tag.X, grid).isFinal());
    grid = "___" + "_X_" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "___" + "_XO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "__X" + "_XO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "_OX" + "_XO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "_XO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "OXO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "OXO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "OXO" + "X__";
    Assert.assertEquals(grid, true, board(Tag.X, grid).isFinal());

    grid = "X__" + "XOO" + "___";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "O_O" + "XOX";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());
    grid = "XOX" + "_X_" + "O_O";
    Assert.assertEquals(grid, false, board(Tag.X, grid).isFinal());

    grid = "O_X" + "__X" + "O_X";
    Assert.assertEquals(grid, true, board(Tag.X, grid).isFinal());
    grid = "X_O" + "_X_" + "O_X";
    Assert.assertEquals(grid, true, board(Tag.X, grid).isFinal());
    grid = "XOX" + "XXO" + "XOO";
    Assert.assertEquals(grid, true, board(Tag.X, grid).isFinal());
  }

  /**
   * Test method for {@link Board#getOnTurn()} and {@link Board.Factory#startState()}.
   */
  @Test
  public void testOnTurn() {

    Board.Factory factory;
    Board board;

    {
      factory = new Board.Factory(Tag.X);

      board = factory.startState();
      Assert.assertEquals(Tag.X, board.getOnTurn());
      board = factory.apply(board, Position.SW);
      Assert.assertEquals(Tag.O, board.getOnTurn());
      board = factory.apply(board, Position.S);
      Assert.assertEquals(Tag.X, board.getOnTurn());
      board = factory.apply(board, Position.SE);
      Assert.assertEquals(Tag.O, board.getOnTurn());
    }

    {
      factory = new Board.Factory(Tag.O);

      board = factory.startState();
      Assert.assertEquals(Tag.O, board.getOnTurn());
      board = factory.apply(board, Position.NE);
      Assert.assertEquals(Tag.X, board.getOnTurn());
      board = factory.apply(board, Position.E);
      Assert.assertEquals(Tag.O, board.getOnTurn());
      board = factory.apply(board, Position.SE);
      Assert.assertEquals(Tag.X, board.getOnTurn());
    }
  }

  /**
   * Test method for {@link Board#getStatus()}.
   */
  @Test
  public void testStatus() {

    Board.Factory factory;
    Board board;

    {
      factory = new Board.Factory(Tag.X);
      board = factory.startState();

      for (Position position : parse(Tag.X, "XOX" + "O_O" + "XOX")) {
        Assert.assertEquals(Status.PLAYING, board.getStatus());
        board = factory.apply(board, position);
      }
      board = factory.apply(board, Position.C);
      Assert.assertEquals(Status.WIN, board.getStatus());
    }

    {
      factory = new Board.Factory(Tag.O);
      board = factory.startState();

      for (Position position : parse(Tag.X, "XXO" + "OOX" + "X_O")) {
        Assert.assertEquals(Status.PLAYING, board.getStatus());
        board = factory.apply(board, position);
      }
      board = factory.apply(board, Position.S);
      Assert.assertEquals(Status.DRAW, board.getStatus());
    }
  }

  /**
   * Test method for {@link Board#tag(Position)} with {@code null} Position.
   */
  @Test(expected = NullPointerException.class)
  public void testTagNull() {
    board(Tag.X).tag(null);
  }

  /**
   * Test method for {@link Board#tag(Position)} and {@link Board.Factory#startState()}.
   */
  @Test
  public void testTagStartState() {

    Board board = board(Tag.X);

    for (Position position : Position.values()) {
      Assert.assertEquals(null, board.tag(position));
    }
  }

  /**
   * Test method for {@link Board#tag(Board.Position)}.
   */
  @Test
  public void testTag() {

    Board board = board(Tag.X, "XXO" + "OOX" + "X__");

    Assert.assertEquals(Tag.X, board.tag(Position.NW));
    Assert.assertEquals(Tag.X, board.tag(Position.N));
    Assert.assertEquals(Tag.X, board.tag(Position.SW));
    Assert.assertEquals(Tag.X, board.tag(Position.E));

    Assert.assertEquals(Tag.O, board.tag(Position.NE));
    Assert.assertEquals(Tag.O, board.tag(Position.C));
    Assert.assertEquals(Tag.O, board.tag(Position.W));

    Assert.assertEquals(null, board.tag(Position.S));
    Assert.assertEquals(null, board.tag(Position.SE));
  }

  /**
   * Test method for {@link Board.Factory#Factory(Tag)} with {@code null} Tag.
   */
  @Test(expected = NullPointerException.class)
  public void testFactoryNullTag() {
    new Board.Factory(null);
  }

  /**
   * Test method for {@link Board.Factory#apply(Board, Position)} with {@code null} Board.
   */
  @Test(expected = NullPointerException.class)
  public void testFactoryApplyNullBoard() {

    // Board factory.
    Board.Factory factory = new Board.Factory(Tag.X);

    factory.apply(null, Position.C);
  }

  /**
   * Test method for {@link Board.Factory#apply(Board, Position)} with {@code null} Position.
   */
  @Test(expected = NullPointerException.class)
  public void testFactoryApplyNullPosition() {

    // Board factory.
    Board.Factory factory = new Board.Factory(Tag.X);

    factory.apply(factory.startState(), null);
  }

  /**
   * @see #board(Tag, Position...)
   * @see #parse(Tag, String)
   * @return Board with positions tagged according to the sequence of positions derived from the grid
   */
  public static Board board(Tag tag, String grid) {
    return board(tag, parse(tag, grid));
  }

  /**
   * This method represents the &delta;*: Q x S* &rarr; Q function.
   * The first argument is a {@link Tag tag} instead of {@link Board start state} (for convenience).
   * @param tag Start tag
   * @param moves Sequence of input symbols
   * @return Board with positions tagged according to specified moves
   * @throws UnsupportedOperationException if board is final but there are unprocessed moves
   * @throws IllegalTurnException if an occupied position is tagged
   */
  public static Board board(Tag tag, Position... moves) {

    // Board factory.
    Board.Factory factory = new Board.Factory(tag);

    // Initial board configuration.
    Board board = factory.startState();

    // Add tags according to the grid snapshot string.
    for (Position position : moves) {
      board = factory.apply(board, position);
    }

    return board;
  }

  /**
   * Parse grid to a sequence of positions.
   * @param startTag Start tag
   * @param grid String representation of a grid
   * @return Sequence of positions derived from the grid
   */
  public static Position[] parse(Tag startTag, String grid) {

    int[] ix = new int[Tag.values().length];
    ix[Tag.X.index()] = startTag.index();
    ix[Tag.O.index()] = startTag.next().index();

    int posLength = 0;
    Position[] pos = new Position[Position.values().length];

    for (Position p : Position.values()) {
      try {

        Tag tag = Tag.valueOf(String.valueOf(grid.charAt(GridTest.index(p))));

        pos[ix[tag.index()]] = p;
        ix[tag.index()] += 2;

        posLength++;

      } catch (IllegalArgumentException e) {
        // 'No enum constant' ignored
      }
    }

    if (Math.abs(ix[Tag.X.index()] - ix[Tag.O.index()]) > 1)
      throw new IllegalArgumentException(startTag + ": " + grid);

    return Arrays.copyOf(pos, posLength);
  }
}
