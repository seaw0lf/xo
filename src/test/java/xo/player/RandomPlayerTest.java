package xo.player;

import static org.hamcrest.CoreMatchers.*;

import java.util.*;

import org.junit.*;

import xo.match.Board;
import xo.match.Grid.Position;
import xo.player.impl.RandomAutonomousPlayer;
import xo.match.BoardTest;

public class RandomPlayerTest {
  private static RandomAutonomousPlayer PLAYER;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER = new RandomAutonomousPlayer();
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    PLAYER.close();
  }

  /**
   * Test method for {@link RandomAutonomousPlayer#apply(Board)}.
   */
  @Test
  public void onEmptyBoard() {

    // Initial board configuration.
    Board board = new Board.Factory(Tag.O).startState();

    EnumSet<Position> pos = EnumSet.noneOf(Position.class);

    for (int i = 0; i < 10; i++) {
      Position position = PLAYER.apply(board);
      Assert.assertNotNull("Never give up! Board is empty...", position);
      pos.add(position);
    }

    Assert.assertTrue("Random player lacks imagination", pos.size() > 3);
  }

  /**
   * Test method for {@link RandomAutonomousPlayer#apply(Board)}.
   */
  @Test
  public void onNonEmptyBoard() {
    Assert.assertThat(PLAYER.apply(BoardTest.board(Tag.X, "_XO" + "O_X" + "X_O")), anyOf(is(Position.NW), is(Position.C), is(Position.S)));
  }

  /**
   * Test method for {@link RandomAutonomousPlayer#apply(Board)}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void onFullBoard() {
    PLAYER.apply(BoardTest.board(Tag.X, "XXO" + "OXX" + "XOO"));
  }
}
