package xo.player;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static xo.match.BoardTest.board;

import org.junit.*;

import xo.match.Board;
import xo.match.Grid.Position;
import xo.player.impl.MinimaxAutonomousPlayer;

public class MinimaxPlayerTest {
  private static Tag TAG;
  private static MinimaxAutonomousPlayer PLAYER;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER = new MinimaxAutonomousPlayer();
    PLAYER.init(TAG = Tag.X);
    PLAYER.boardListener().automatonStarted(new Board.Factory(TAG).startState());
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    PLAYER.close();
  }

  /**
   * Test method for {@link MinimaxAutonomousPlayer#apply(Board)}.
   * Win in 3 moves.
   */
  @Test
  public void win3() {
    // O_X
    // O_X
    // __X
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "O_X" + "___")), is(Position.SE));
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "O__" + "__X")), is(Position.E));
    Assert.assertThat(PLAYER.apply(board(TAG, "O__" + "O_X" + "__X")), is(Position.NE));
    // __X
    // _OX
    // O_X
    Assert.assertThat(PLAYER.apply(board(TAG, "__X" + "_OX" + "O__")), is(Position.SE));
    Assert.assertThat(PLAYER.apply(board(TAG, "__X" + "_O_" + "O_X")), is(Position.E));
    Assert.assertThat(PLAYER.apply(board(TAG, "___" + "_OX" + "O_X")), is(Position.NE));
    // O_X
    // _X_
    // X_O
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "_X_" + "__O")), is(Position.SW));
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "___" + "X_O")), is(Position.C));
    Assert.assertThat(PLAYER.apply(board(TAG, "O__" + "_X_" + "X_O")), is(Position.NE));
    // __X
    // _XO
    // X_O
    Assert.assertThat(PLAYER.apply(board(TAG, "__X" + "_XO" + "__O")), is(Position.SW));
    Assert.assertThat(PLAYER.apply(board(TAG, "__X" + "__O" + "X_O")), is(Position.C));
    Assert.assertThat(PLAYER.apply(board(TAG, "___" + "_XO" + "X_O")), is(Position.NE));
  }

  /**
   * Test method for {@link MinimaxAutonomousPlayer#apply(Board)}.
   * Win in 4 moves.
   */
  @Test
  public void win4() {
    // XX_
    // OXO
    // O__
    Assert.assertThat(PLAYER.apply(board(TAG, "XX_" + "OXO" + "O__")), anyOf(is(Position.S), is(Position.SE), is(Position.NE)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XXO" + "OX_" + "O__")), anyOf(is(Position.S), is(Position.SE)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XX_" + "OX_" + "OO_")), anyOf(is(Position.NE), is(Position.SE)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XX_" + "OX_" + "O_O")), anyOf(is(Position.NE), is(Position.S)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XX_" + "OX_" + "_OO")), is(Position.NE));
    Assert.assertThat(PLAYER.apply(board(TAG, "XXO" + "OX_" + "_O_")), is(Position.SE));
    Assert.assertThat(PLAYER.apply(board(TAG, "XXO" + "OX_" + "__O")), is(Position.S));
    // X_X
    // OXO
    // _O_
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "OXO" + "_O_")), anyOf(is(Position.N), is(Position.SE), is(Position.SW)));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "OXO" + "O__")), anyOf(is(Position.N), is(Position.SE)));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "OXO" + "__O")), anyOf(is(Position.N), is(Position.SW)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "OXO" + "___")), anyOf(is(Position.SE), is(Position.SW)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "OX_" + "__O")), is(Position.SW));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "OX_" + "O__")), is(Position.SE));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "OX_" + "O_O")), is(Position.N));
    // X_X
    // O__
    // OOX
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "O__" + "OOX")), anyOf(is(Position.N), is(Position.C), is(Position.E)));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "_O_" + "OOX")), anyOf(is(Position.N), is(Position.E)));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "__O" + "OOX")), anyOf(is(Position.N), is(Position.C)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "___" + "OOX")), anyOf(is(Position.C), is(Position.E)));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "__O" + "O_X")), is(Position.C));
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "_O_" + "O_X")), is(Position.E));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_X" + "_OO" + "O_X")), is(Position.N));
  }

  /**
   * Test method for {@link MinimaxAutonomousPlayer#apply(Board)}.
   * Block in 3rd move.
   */
  @Test
  public void block3() {
    // OX_
    // O_X
    // O__
    Assert.assertThat(PLAYER.apply(board(TAG, "OX_" + "O_X" + "___")), is(Position.SW));
    Assert.assertThat(PLAYER.apply(board(TAG, "OX_" + "__X" + "O__")), is(Position.W));
    Assert.assertThat(PLAYER.apply(board(TAG, "_X_" + "O_X" + "O__")), is(Position.NW));
    // O_X
    // O__
    // OX_
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "O__" + "_X_")), is(Position.SW));
    Assert.assertThat(PLAYER.apply(board(TAG, "O_X" + "___" + "OX_")), is(Position.W));
    Assert.assertThat(PLAYER.apply(board(TAG, "__X" + "O__" + "OX_")), is(Position.NW));
    // __O
    // XO_
    // OX_
    Assert.assertThat(PLAYER.apply(board(TAG, "___" + "XO_" + "OX_")), is(Position.NE));
    Assert.assertThat(PLAYER.apply(board(TAG, "__O" + "X__" + "OX_")), is(Position.C));
    Assert.assertThat(PLAYER.apply(board(TAG, "__O" + "XO_" + "_X_")), is(Position.SW));
    // X_O
    // _OX
    // O__
    Assert.assertThat(PLAYER.apply(board(TAG, "X__" + "_OX" + "O__")), is(Position.NE));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_O" + "__X" + "O__")), is(Position.C));
    Assert.assertThat(PLAYER.apply(board(TAG, "X_O" + "_OX" + "___")), is(Position.SW));
  }

  /**
   * Test method for {@link MinimaxAutonomousPlayer#apply(Board)}.
   * Block in 4th move.
   */
  @Test
  public void block4() {
    // X_O
    // OX_
    // X_O
    Assert.assertThat(PLAYER.apply(board(TAG, "X_O" + "OX_" + "X_O")), is(Position.E));
    // OXO
    // __X
    // X_O
    Assert.assertThat(PLAYER.apply(board(TAG, "OXO" + "__X" + "X_O")), is(Position.C));
    // XOX
    // OO_
    // _X_
    Assert.assertThat(PLAYER.apply(board(TAG, "XOX" + "OO_" + "_X_")), is(Position.E));
    Assert.assertThat(PLAYER.apply(board(TAG, "XO_" + "OOX" + "X__")), is(Position.S));
    // OXX
    // OO_
    // _X_
    Assert.assertThat(PLAYER.apply(board(TAG, "OXX" + "OO_" + "_X_")), anyOf(is(Position.E), is(Position.SW), is(Position.SE)));
    Assert.assertThat(PLAYER.apply(board(TAG, "OX_" + "OOX" + "X__")), is(Position.SE));
  }
}
