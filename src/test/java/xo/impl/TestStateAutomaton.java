package xo.impl;

import automaton.*;
import xo.impl.TestState.Factory;

public class TestStateAutomaton extends StateAutomaton<TestState, TestSymbol> {

  public TestStateAutomaton(InputSupplier<TestState, TestSymbol> inputSupplier) {
    this.inputSupplier = inputSupplier;
  }

  @Override
  protected void open() {
    final Factory factory = new TestState.Factory();
    state = factory.startState();
    transition = factory;
  }

  @Override
  public void close() {
  }
}
