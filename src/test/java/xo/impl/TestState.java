package xo.impl;

import automaton.*;

public final class TestState implements State {
  private boolean isFinal;
  private int index;

  private TestState(boolean isFinal, int index) {
    this.isFinal = isFinal;
    this.index = index;
  }

  public int getIndex() {
    return index;
  }

  @Override
  public boolean isFinal() {
    return isFinal;
  }

  @Override
  public String toString() {
    return index + (isFinal ? "*" : "");
  }

  public static class Factory implements State.Factory<TestState>, TransitionFunction<TestState, TestSymbol> {

    @Override
    public TestState startState() {
      return new TestState(false, 0);
    }

    @Override
    public TestState apply(TestState state, TestSymbol symbol) {
      // epsilon symbol - doesn't change the state
      if (symbol == null)
        return state;
      if (symbol == TestSymbol.ERROR)
        throw new UnsuccessfulStateChangeException();
      if (symbol == TestSymbol.TERMINATOR)
        throw new TerminatedStateAutomatonException();
      return new TestState(symbol == TestSymbol.FINAL ? true : state.isFinal, state.index + 1);
    }
  }
}
