package xo.impl;

import automaton.Symbol;

public enum TestSymbol implements Symbol {

  /**
   * Regular symbol.
   */
  REGULAR,

  /**
   * Final symbol. Works like regular symbol but forces to change the state to a final one.
   */
  FINAL,

  /**
   * Error symbol. Simulates exceptional state during transition function execution.
   */
  ERROR,

  /**
   * Terminator symbol. Simulates termination during transition function execution.
   */
  TERMINATOR;
}
