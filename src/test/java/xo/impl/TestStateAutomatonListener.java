package xo.impl;

import java.util.Map;

import org.junit.Assert;

import automaton.StateAutomatonListener;

/**
 * Tests whether the specified events are invoked in specified order.
 *
 */
public final class TestStateAutomatonListener implements AutoCloseable, StateAutomatonListener<TestState> {
  private Map<Event, Integer> expectedOrder;
  private int counter = 0;

  /** Listener events. */
  public enum Event {
    automatonStarted, automatonStopped, automatonTerminated, beforeStateChange, afterStateChange, unsuccessfulStateChange,
  }

  /**
   * Test listener constructor.
   * @param order Events and their order to be tested
   */
  public TestStateAutomatonListener(Map<Event, Integer> order) {
    this.expectedOrder = order;
  }

  /**
   * Checks event invocation order against expected order.
   * @param event {@link Event Listener event}
   */
  private void assertEvent(Event event) {
    Integer ord = expectedOrder.get(event);
    if (ord != null) {
      counter++;
      Assert.assertEquals(event + " must be executed " + ord, ord.intValue(), counter);
    } else {
      Assert.fail(event + " must not be executed");
    }
  }

  @Override
  public void automatonStarted(TestState startState) {
    assertEvent(Event.automatonStarted);
  }

  @Override
  public void beforeStateChange(TestState state) {
    assertEvent(Event.beforeStateChange);
  }

  @Override
  public void unsuccessfulStateChange(TestState state, Throwable cause) {
    assertEvent(Event.unsuccessfulStateChange);
  }

  @Override
  public void afterStateChange(TestState state) {
    assertEvent(Event.afterStateChange);
  }

  @Override
  public void automatonTerminated(TestState state, Throwable cause) {
    assertEvent(Event.automatonTerminated);
  }

  @Override
  public void automatonStopped(TestState finalState) {
    assertEvent(Event.automatonStopped);
  }

  @Override
  public void close() throws AssertionError {
    Assert.assertEquals("last call not executed", expectedOrder.size(), counter);
  }
}
