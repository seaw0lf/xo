package xo.impl;

import automaton.InputSupplier;

/**
 * Supplies the given {@link #TestSupplier(TestSymbol...) sequence of input symbols}.
 *
 */
public final class TestSupplier implements InputSupplier<TestState, TestSymbol> {
  private int index;
  private TestSymbol[] inputSequence;

  /**
   * Supplier constructor.
   * @param inputSequence Sequence of input symbols to supply
   */
  public TestSupplier(TestSymbol... inputSequence) {
    this.index = 0;
    this.inputSequence = inputSequence;
  }

  @Override
  public TestSymbol apply(TestState state) {
    return index < inputSequence.length ? inputSequence[index++] : null;
  }
}
