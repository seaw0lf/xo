package xo.player;

import automaton.*;
import xo.competition.*;
import xo.match.*;
import xo.match.Grid.Position;

/**
 * Represents {@link Competition competition}/{@link Match match} player.
 * Player is an auto-closeable {@link InputSupplier input supplier} for a {@link Match match automaton}.
 * <p>
 * Player instance is associated with and referred to using a {@link Tag tag} during the competition/match.
 * 
 * @see AutoCloseable
 * @see InputSupplier
 *
 */
// Unfortunately, due to type erasure it is impossible for Player to implement StateAutomatonListener<Board> and
// StateAutomatonListener<Score> at once. Thus, extra methods for board & score listener shall exist :(
public interface Player extends AutoCloseable, InputSupplier<Board, Position> {

  /**
   * Instantiates a player.
   * @param playerClassName Qualified player implementation name
   * @param tag Player tag
   * @return Initialized player
   * @throws Exception exception during player instantiation
   */
  static Player forName(String playerClassName, Tag tag) throws Exception {
    return forName(Class.forName(playerClassName).asSubclass(Player.class), tag);
  }

  /**
   * Instantiates a player.
   * @param playerClass Player implementation class
   * @param tag Player tag
   * @return Initialized player
   * @throws Exception exception during player instantiation
   */
  static Player forName(Class<? extends Player> playerClass, Tag tag) throws Exception {
    Player player = playerClass.newInstance();
    try {
      player.init(tag);
    } catch (Throwable t) {
      try {
        player.close();
      } catch (Throwable tClose) {
        t.addSuppressed(tClose);
      }
      throw t;
    }
    return player;
  }

  /**
   * Player initialization.
   * @param tag Player tag
   * @throws Exception exception during initialization
   */
  default void init(Tag tag) throws Exception {
  }

  /**
   * Provides {@link StateAutomatonListener board listener} required for a {@link Match match}.
   * @return Board listener
   */
  default StateAutomatonListener<Board> boardListener() {
    return null;
  }

  /**
   * Provides {@link StateAutomatonListener score listener} required for a {@link Competition competition}.
   * @return Score listener
   */
  default StateAutomatonListener<Score> scoreListener() {
    return null;
  }
}
