package xo.player;

import automaton.Symbol;

/**
 * Defines player tags.
 * <p>
 * Tags are typically used for
 * <ul>
 * <li>player identification,</li>
 * <li>grid tagging.</li>
 * </ul>
 * <p>
 * Tag is a {@link xo.competition.Competition competition automaton} {@link Symbol input symbol}.
 *
 * @see Symbol
 *
 */
public enum Tag implements Symbol {

  /**
   * Cross.
   */
  X(0),

  /**
   * Nought.
   */
  O(1);

  /**
   * Defines order, e.g for array indexing.
   */
  private int index;

  /**
   * Creates a tag with specific order.
   * @param index Order
   */
  private Tag(int index) {
    this.index = index;
  }

  /**
   * Returns tag order.
   * @return Order of the tag
   */
  public int index() {
    return index;
  }

  /**
   * Returns next tag after this tag (X &rarr; O, O &rarr; X).
   * @return Next tag
   */
  public Tag next() {
    switch (this) {
      case X:
        return O;
      case O:
        return X;
      default:
        throw new IllegalStateException("Unknown player tag " + this);
    }
  }
}
