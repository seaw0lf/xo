package xo.player.impl;

import java.util.Random;

import xo.match.Board;
import xo.match.Grid.Position;
import xo.player.Player;

/**
 * Simple autonomous player.
 * <p>
 * Uses {@link Random random number generator} to pick a vacant {@link Position grid position}.
 * 
 * @see Player
 *
 */
public final class RandomAutonomousPlayer implements Player {
  /** Random number generator. */
  private static final Random RANDOM = new Random();

  @Override
  public Position apply(Board board) {

    if (board.emptyPositions() == 0)
      throw new IllegalArgumentException("No vacant positions left");

    int ith = RANDOM.nextInt(board.emptyPositions());

    // Return i-th vacant position.
    for (Position pos : Position.values()) {
      if (board.tag(pos) == null) {
        if (ith-- == 0) {
          return pos;
        }
      }
    }

    throw new IllegalStateException("Too stupid to pick a random vacant position");
  }

  @Override
  public void close() {
    // Nothing to close
  }
}
