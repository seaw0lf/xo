package xo.player.impl;

import java.util.Objects;

import automaton.StateAutomatonListener;
import xo.competition.Score;
import xo.match.*;
import xo.match.Grid.Position;
import xo.player.*;
import xo.player.impl.XOWindow.Outcome;

/**
 * Swing/GUI player.
 * <p>
 * Provides a lightweight GUI with a grid and a score board.
 * 
 * @see Player
 *
 */
public final class SwingInteractivePlayer implements Player {

  /**
   * This player's tag.
   */
  private Tag tag;

  /**
   * Graphical interface for the user.
   */
  private XOWindow window;

  @Override
  public void init(Tag tag) throws Exception {
    this.tag = Objects.requireNonNull(tag);

    // Show XO-Window.
    window = XOWindow.newInstanceEDT(tag);
  }

  @Override
  public Position apply(Board board) {
    return window.positionEDT(board);
  }

  @Override
  public StateAutomatonListener<Board> boardListener() {
    return new StateAutomatonListener<Board>() {

      @Override
      public void automatonStopped(Board finalState) {
        // Show the match outcome for the player.
        switch (finalState.getStatus()) {
          case DRAW:
            window.outcomeDialogEDT(Outcome.DRAW, "Draw", finalState);
            break;
          case WIN:
            final boolean won = finalState.getOnTurn() == tag.next();
            window.outcomeDialogEDT(won ? Outcome.WIN : Outcome.LOSS, won ? "Win" : "Loss", finalState);
            break;
          default:
            break;
        }
      }

      @Override
      public void automatonTerminated(Board state, Throwable cause) {
        // Show message if the other player quits.
        if (state.getOnTurn() != tag && cause instanceof QuitException) {
          window.messageDialogEDT(Outcome.QUIT, "Opponent quits...");
        }
      }
    };
  }

  @Override
  public StateAutomatonListener<Score> scoreListener() {
    return new StateAutomatonListener<Score>() {
      @Override
      public void afterStateChange(Score state) {
        // Update score after each match.
        window.updateScoreEDT(state);
      }
    };
  }

  @Override
  public void close() {
    if (window != null) {
      window.closeEDT();
    }
  }
}
