package xo.player.impl;

import java.util.*;

import automaton.StateAutomatonListener;
import xo.match.*;
import xo.match.Grid.Position;
import xo.player.*;

/**
 * Minimax autonomous player.
 * <p>
 * Uses {@link Minimax minimax algorithm} to pick a vacant {@link Position grid position}.
 * 
 * @see Player
 *
 */
public final class MinimaxAutonomousPlayer implements Player {
  /**
   * Random number generator.
   */
  private static final Random RANDOM = new Random();

  /**
   * This player's tag.
   */
  private Tag tag;

  /**
   * Player's long-term memory. Remains the same (during the life of this class).
   * <p>
   * Represents knowledge of the player which constitutes of all optimal moves per each possible {@link Grid grid configuration} per each possible <i>match configuration</i>.
   * <i>Match configuration</i> is a tuple containing tag of this player and tag of the player who starts the match.
   * <p>
   * Contains 4 {@link Map maps} (one per each match configuration) which are referred by {@link #shortTermMemory}:<br>
   * Map&lt;this player's tag, Map&lt;first player's tag in a match, Map&lt;grid configuration, list of all optimal moves for a grid configuration&gt;&gt;&gt;
   */
  private static final Map<Tag, Map<Tag, Map<Grid, List<Position>>>> longTermMemory;

  /**
   * Player's short-term memory. Remains the same during one match.
   * <p>
   * Represents knowledge of the player which constitutes of all optimal moves per each possible grid configuration per current game configuration.
   * <p>
   * Contains {@link List list} of all optimal moves per {@link Grid grid configuration}.
   */
  private Map<Grid, List<Position>> shortTermMemory;

  static {
    // "Time moves in one direction, memory in another." -- William Gibson
    longTermMemory = gainKnowledge();
  }

  /**
   * Runs the {@link Minimax minimax algorithm} for each <i>match configuration</i>.
   * @return Knowlege of how to play a Tic-Tac-Toe Match
   */
  private static Map<Tag, Map<Tag, Map<Grid, List<Position>>>> gainKnowledge() {

    Map<Tag, Map<Tag, Map<Grid, List<Position>>>> knowledge = new EnumMap<>(Tag.class);

    // From each player's point of view and...
    for (Tag playerTag : Tag.values()) {
      Map<Tag, Map<Grid, List<Position>>> playerKnowledge = new HashMap<>();
      // ... for each starting player...
      for (Tag firstPlayerTag : Tag.values()) {
        // ... compute optimal moves.
        Map<Grid, List<Position>> matchKnowledge = new HashMap<>();
        Minimax.perform(matchKnowledge, playerTag, new Grid(), firstPlayerTag, 0, new HashMap<>());
        playerKnowledge.put(firstPlayerTag, Collections.unmodifiableMap(matchKnowledge));
      }
      knowledge.put(playerTag, Collections.unmodifiableMap(playerKnowledge));
    }

    return Collections.unmodifiableMap(knowledge);
  }

  @Override
  public void init(Tag tag) throws Exception {
    this.tag = Objects.requireNonNull(tag);
  }

  @Override
  public Position apply(Board board) {
    // Recall optimal moves for the current grid configuration of the board.
    List<Position> optimalMoves = shortTermMemory.get(board.getGrid());
    // Choose one at random.
    return optimalMoves.get(RANDOM.nextInt(optimalMoves.size()));
  }

  @Override
  public StateAutomatonListener<Board> boardListener() {
    return new StateAutomatonListener<Board>() {
      @Override
      public void automatonStarted(Board initialState) {
        // A match starts. Board contains first player's identification.
        // Thus, it is feasible to update short-term memory with the appropriate game playing skill.
        shortTermMemory = longTermMemory.get(tag).get(initialState.getOnTurn());
      }
    };
  }

  @Override
  public void close() {
    // Nothing to close
  }

  /**
   * The <a href="https://en.wikipedia.org/wiki/Minimax#Pseudocode">Minimax algorithm</a> implementation.
   * Core idea is to minimize the possible loss for a worst case (maximum loss) scenario in a two-player
   * game where players take alternate moves.
   *
   */
  private static final class Minimax {

    /** Minimax grid status. */
    private enum Status {
      /** Match in progress. */
      PLAYING,
      /** Draw match. */
      DRAW,
      /** Winning match for player X. */
      WIN_X,
      /** Winning match for player O. */
      WIN_O;

      /** Winning status for particular {@link Tag tag}. */
      public static final Map<Tag, Status> WIN;
      static {
        WIN = new EnumMap<>(Tag.class);
        WIN.put(Tag.X, WIN_X);
        WIN.put(Tag.O, WIN_O);
      }

      /**
       * Determines score for minimax algorithm according to the state of a hypothetical grid configuration:
       * <pre>
       *  10 = instant win
       *   9 = win after 1st move
       *    ...
       *   1 = win after 9th move
       *   0 = draw
       *  -1 = loss after 9th move
       *    ...
       *  -9 = loss after 1st move
       * -10 = instant loss
       * </pre>
       * @param tag Identification of this player
       * @param level Level of the hypothetical grid configuration in the state-space tree a.k.a. number of moves
       * @return Score of the current status with respect to this player and number of moves
       */
      private int score(Tag tag, int level) {
        switch (this) {
          case DRAW:
            return 0;
          case WIN_X:
            return (tag == Tag.X ? 10 - level : -10 + level);
          case WIN_O:
            return (tag == Tag.O ? 10 - level : -10 + level);
          default:
            throw new UnsupportedOperationException("Status: " + this);
        }
      }

      /**
       * Determines status of a grid.
       * @param grid Grid
       * @return Status
       */
      private static Status of(Grid grid) {
        for (List<Position> row : Position.ROWS) {
          Tag rowTag = grid.allInRow(row);
          if (rowTag != null) {
            return Status.WIN.get(rowTag);
          }
        }
        return grid.emptyPositions() == 0 ? Status.DRAW : Status.PLAYING;
      }
    }

    /**
     * Computes {@link Position appropriate moves} per each feasible {@link Grid grid configuration}.
     * @param positionMemory Storage for moves per grid configuration. <b>Result of the algorithm.</b>
     * @param thisTag This player's identification
     * @param grid Grid configuration to be explored
     * @param currentTag Current player's identification
     * @param level Level of the grid configuration in the state-space tree a.k.a. number of moves
     * @param scoreMemory Memoization facility (for remembering score per grid configuration)
     * @return Grid score
     */
    static int perform(Map<Grid, List<Position>> positionMemory, Tag thisTag, Grid grid, Tag currentTag, int level, Map<Grid, Integer> scoreMemory) {

      { // Search score memory first.
        Integer score = scoreMemory.get(grid);
        if (score != null)
          return score;
      }

      // Basis.
      // Grid is full | winner is known.
      Status status = Status.of(grid);
      if (status != Status.PLAYING) {
        int score = status.score(thisTag, level);
        scoreMemory.put(grid, score);
        return score;
      }

      // Induction.
      // There are positions to explore.

      // Compute score for each free position.
      // SLOW Arrays.stream(Position.values()).filter(p -> grid.tag(p) == null).collect(Collectors.toMap(Function.identity(), p -> perform(positionMemory, thisTag, grid.tag(p, currentTag), currentTag.next(), level + 1, scoreMemory)));
      Map<Position, Integer> scorePerPosition = new EnumMap<>(Position.class);
      for (Position position : Position.values()) {
        if (grid.tag(position) == null) {
          scorePerPosition.put(position, perform(positionMemory, thisTag, grid.tag(position, currentTag), currentTag.next(), level + 1, scoreMemory));
        }
      }
      // Group positions by score (using a TreeMap to leverage natural order).
      // SLOW scorePerPosition.entrySet().stream().collect(Collectors.groupingBy(Map.Entry::getValue, TreeMap::new, Collectors.mapping(Map.Entry::getKey, Collectors.toList())));
      TreeMap<Integer, List<Position>> groupByScore = new TreeMap<>();
      for (Map.Entry<Position, Integer> entry : scorePerPosition.entrySet()) {
        List<Position> positions = groupByScore.get(entry.getValue());
        if (positions == null) {
          groupByScore.put(entry.getValue(), positions = new ArrayList<>(Position.values().length));
        }
        positions.add(entry.getKey());
      }
      // Select all positions with maximal/minimal score.
      Map.Entry<Integer, List<Position>> minMax = currentTag == thisTag ? /* max */ groupByScore.lastEntry() : /* min */ groupByScore.firstEntry();

      positionMemory.put(grid, minMax.getValue());
      scoreMemory.put(grid, minMax.getKey());
      return minMax.getKey();
    }
  }
}
