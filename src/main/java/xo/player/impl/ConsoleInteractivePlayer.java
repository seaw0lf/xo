package xo.player.impl;

import java.io.*;
import java.util.*;

import automaton.StateAutomatonListener;
import xo.competition.Score;
import xo.match.*;
import xo.match.Grid.Position;
import xo.player.*;

/**
 * Simple console player.
 * Reads from {@link System#in standard input} and writes to {@link System#out standard output}.
 * <p>
 * Considers numeric keypad to be a Tic-Tac-Toe grid:
 * <pre>
 *  7 | 8 | 9 
 * ---+---+---
 *  4 | 5 | 6 
 * ---+---+---
 *  1 | 2 | 3 </pre>
 * 
 * @see Player
 *
 */
public class ConsoleInteractivePlayer implements Player {

  /**
   * This player's tag.
   */
  protected Tag tag;

  /**
   * Player input.
   */
  protected Scanner in;

  /**
   * Player output.
   */
  protected PrintWriter out;

  @Override
  public void init(Tag tag) throws IOException {
    this.tag = Objects.requireNonNull(tag);
    initIO();
  }

  /**
   * Initialize player input and output.
   * @throws IOException iff input/output setup fails
   */
  protected void initIO() throws IOException {

    in = new Scanner(System.in);
    out = new PrintWriter(System.out, true);

    out.println("Tic-Tac-Toe");
    out.println("Console Client");
    out.println("Player: " + tag);
  }

  @Override
  public Position apply(Board board) {
    // Print current board.
    out.println(board);
    // Read input.
    while (true) {
      out.print(board.getOnTurn() + " ([1]-[9], [Q]uit)> ");
      out.flush();

      String input = in.nextLine().toUpperCase();
      switch (input.charAt(input.length() - 1)) {
        case '1':
          return Position.SW;
        case '2':
          return Position.S;
        case '3':
          return Position.SE;
        case '4':
          return Position.W;
        case '5':
          return Position.C;
        case '6':
          return Position.E;
        case '7':
          return Position.NW;
        case '8':
          return Position.N;
        case '9':
          return Position.NE;
        case 'Q':
        case 'T':
          return null;
        default:
          // Illegal input. Let's try again...
      }
    }
  }

  @Override
  public StateAutomatonListener<Board> boardListener() {
    return new StateAutomatonListener<Board>() {

      @Override
      public void unsuccessfulStateChange(Board state, Throwable cause) {
        // Print message on unsuccessful state change of this player due to illegal turn.
        if (state.getOnTurn() == tag && cause instanceof IllegalTurnException) {
          out.println("Illegal turn. Try again!");
        }
      }

      @Override
      public void automatonStopped(Board finalState) {
        // Print the final board.
        out.println(finalState);
        // Print the match outcome for the player.
        switch (finalState.getStatus()) {
          case DRAW:
            out.println("*** DRAW ***");
            break;
          case WIN:
            // If it's opponents turn now, this player won in previous turn.
            out.println(finalState.getOnTurn() != tag ? "*** WIN ***" : "*** LOSS ***");
            break;
          default:
            out.println("?!?");
            break;
        }
      }

      @Override
      public void automatonTerminated(Board state, Throwable cause) {
        // Print message if the other player quits.
        if (state.getOnTurn() != tag && cause instanceof QuitException) {
          out.println(cause.getLocalizedMessage());
        }
      }
    };
  }

  @Override
  public StateAutomatonListener<Score> scoreListener() {
    return new StateAutomatonListener<Score>() {
      @Override
      public void afterStateChange(Score state) {
        // Print score after each match.
        out.println("Score: " + state);
      }
    };
  }

  @Override
  public void close() {
    if (out != null) {
      out.close();
    }
    if (in != null) {
      in.close();
    }
  }
}
