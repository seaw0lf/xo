package xo.player.impl;

import java.awt.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.*;

import xo.competition.Score;
import xo.match.Board;
import xo.match.Grid.Position;
import xo.player.Tag;

/**
 * Window containing scoreboard (tag icons with score) and a grid.
 *
 */
final class XOWindow extends JFrame {
  private static final long serialVersionUID = 1;

  /**
   * Match outcome.
   */
  enum Outcome {
    WIN("win.png"), DRAW("draw.png"), LOSS("loss.png"), QUIT("quit.png");

    /** Resource URL. */
    private URL url;

    Outcome(String name) {
      url = getClass().getClassLoader().getResource("outcome/" + name);
    }

    @Override
    public String toString() {
      return url.toString();
    }
  }

  /** Background image - a grid. */
  private static final ImageIcon BACKGROUND;
  /** Tag icon. */
  private static final Map<Tag, ImageIcon> ICON;
  /** Tag color. */
  private static final Map<Tag, Color> ICON_COLOR;

  static {
    ClassLoader cl = XOWindow.class.getClassLoader();

    BACKGROUND = new ImageIcon(cl.getResource("grid.png"));

    ICON = new EnumMap<>(Tag.class);
    ICON.put(Tag.X, new ImageIcon(cl.getResource("tag/X.png")));
    ICON.put(Tag.O, new ImageIcon(cl.getResource("tag/O.png")));

    ICON_COLOR = new EnumMap<>(Tag.class);
    ICON_COLOR.put(Tag.X, Color.decode("#FF5B4E"));
    ICON_COLOR.put(Tag.O, Color.decode("#4A7DCA"));
  }

  /** Current player's tag. */
  private Tag tag;
  /** Player-on-turn indicator. */
  private Map<Tag, JLabel> players = new EnumMap<>(Tag.class);
  /** Score indicator. */
  private Map<Tag, JLabel> scores = new EnumMap<>(Tag.class);
  /** Buttons. One for each position. */
  private Map<Position, JButton> buttons = new EnumMap<>(Position.class);

  /** An easy-to-use 'pipe' to communicate between main-XO and AWT-EventQueue thread. */
  // Better would be /new SynchronousQueue<>()/ but SQ can be offered with element only when someone is taking.
  // In situation when player quits the competition while opponent is making the move
  // it is required to offer (during dispose) without taking beforehand.
  private BlockingQueue<Object> positionQueue = new ArrayBlockingQueue<>(1);

  /**
   * XO-Window constructor.
   * @param tag Current player
   */
  private XOWindow(Tag tag) {
    this.tag = Objects.requireNonNull(tag);
  }

  /**
   * Static XO-Window factory.
   * @param tag Current player
   * @return XO-Window
   * @see #initUI()
   */
  static XOWindow newInstanceEDT(Tag tag) {
    XOWindow window = new XOWindow(tag);
    try {
      EventQueue.invokeAndWait(() -> {
        window.initUI();
        window.setVisible(true);
      });
    } catch (Throwable t) {
      throw new RuntimeException(t);
    }
    return window;
  }

  /**
   * Updates grid, player on turn, and waits for user's input.
   * @param board Current board
   * @return Player's move, i.e. position to be tagged
   * @see #position()
   */
  Position positionEDT(Board board) {
    EventQueue.invokeLater(() -> updateOnTurn(tag));
    try {
      EventQueue.invokeLater(() -> updateGrid(board));
      return position();
    } finally {
      EventQueue.invokeLater(() -> updateOnTurn(tag.next()));
    }
  }

  /**
   * Updates player score.
   * @param score Current player score
   */
  void updateScoreEDT(Score score) {
    EventQueue.invokeLater(() -> updateScore(score));
  }

  /**
   * Shows game outcome. Then cleans the grid.
   * @param outcome Game outcome
   * @param message Dialog message
   * @param finalState Current (final) board
   */
  void outcomeDialogEDT(Outcome outcome, String message, Board finalState) {

    EventQueue.invokeLater(() -> updateGrid(finalState));

    if (outcome == Outcome.WIN || outcome == Outcome.LOSS) {
      EventQueue.invokeLater(() -> highlightWin(finalState));
    }

    messageDialogEDT(outcome, message);

    EventQueue.invokeLater(() -> cleanGrid());
  }

  /**
   * Shows message dialog.
   * @param outcome Game outcome
   * @param message Dialog message
   */
  void messageDialogEDT(Outcome outcome, String message) {
    try {
      EventQueue.invokeAndWait(() -> JOptionPane.showMessageDialog(this, "<html><body><img src=\"" + outcome + "\" /></body></html>", message, JOptionPane.PLAIN_MESSAGE));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Releases all of the resources used by this XO-Window.
   */
  void closeEDT() {
    EventQueue.invokeLater(() -> dispose());
  }

  /**
   * Sets the Look &amp; Feel.
   */
  private void initUI() {
    // Title and icon.
    setTitle("Tic-Tac-Toe (" + tag + ")");
    setIconImage(ICON.get(tag).getImage());
    // Disable frame resizing.
    setResizable(false);
    // Center the frame.
    setLocationRelativeTo(null);
    // Close windows on close button click.
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    // Frame layout.
    setLayout(new BorderLayout());

    { // Info-panel

      for (Tag t : Tag.values()) {
        // Player icon.
        JLabel playerLabel = new JLabel(ICON.get(t));
        playerLabel.setPreferredSize(new Dimension(150, 130));
        players.put(t, playerLabel);
        // Player Score.
        JLabel scoreLabel = new JLabel("0");
        scoreLabel.setFont(new Font(scoreLabel.getFont().getName(), Font.BOLD, 48));
        scoreLabel.setForeground(ICON_COLOR.get(t));
        scores.put(t, scoreLabel);
      }
      // No player on turn.
      updateOnTurn(null);

      add(players.get(Tag.X), BorderLayout.WEST);
      add(players.get(Tag.O), BorderLayout.EAST);

      JPanel sp = new JPanel(new BorderLayout());
      sp.add(scores.get(Tag.X), BorderLayout.WEST);
      sp.add(scores.get(Tag.O), BorderLayout.EAST);

      add(sp, BorderLayout.CENTER);
    }

    { // Grid
      JPanel panel = new JPanel(new GridLayout(3, 3)) {
        private static final long serialVersionUID = 1;
        @Override
        protected void paintComponent(Graphics gfx) {
          super.paintComponent(gfx);
          gfx.drawImage(BACKGROUND.getImage(), 0, 0, null);
        }
      };
      for (Position pos : Position.values()) {
        JButton button = new JButton();
        button.setEnabled(false);
        button.setFocusable(false);
        button.setBorderPainted(false);
        button.setContentAreaFilled(false);
        button.setPreferredSize(new Dimension(150, 130));
        // On button click...
        button.addActionListener(e -> {
          final JButton btn = (JButton) e.getSource();
          btn.setEnabled(false);
          positionQueue.add(pos);
          btn.setIcon(ICON.get(tag));
        });
        panel.add(button);
        buttons.put(pos, button);
      }
      add(panel, BorderLayout.SOUTH);
    }

    pack();
  }

  /**
   * Cleans the grid.
   */
  private void cleanGrid() {
    // Remove icon & disable each button.
    buttons.values().stream().forEach(b -> {
      b.setEnabled(false);
      b.setIcon(null);
    });
  }

  /**
   * Updates grid according to the match board.
   * @param board Current board
   */
  private void updateGrid(Board board) {
    // Update each button's icon according to the match board.
    buttons.entrySet().stream().forEach(e -> {
      Tag tag = board.tag(e.getKey());
      e.getValue().setIcon(tag == null ? null : ICON.get(tag));
    });
  }

  /**
   * Updates score indicator according to the competition score.
   * @param score Current score
   */
  private void updateScore(Score score) {
    // Update each player's score according to the competition score.
    scores.entrySet().stream().forEach(e -> e.getValue().setText(String.valueOf(score.getScore(e.getKey()))));
  }

  /**
   * Updates player-on-turn indicator according to the tag.
   * @param tag Tag
   */
  private void updateOnTurn(Tag tag) {
    // Highlight the player with given tag.
    players.entrySet().stream().forEach(e -> e.getValue().setEnabled(e.getKey() == tag));
  }

  /**
   * Highlights the winning row of the current match.
   * @param finalState Final board of a match
   */
  private void highlightWin(Board finalState) {
    final Tag winner = finalState.getOnTurn().next();
    // From each possible row...
    Position.ROWS.stream()
      // ... retain those constituted of winner's tag only...
      .filter(row -> row.stream().map(p -> finalState.tag(p)).allMatch(tag -> tag == winner))
      // ... and highlight them.
      .forEach(row -> row.stream().forEach(pos -> buttons.get(pos).setEnabled(true)));
  }

  /**
   * Waits for user's input.
   * @return Position to be tagged
   */
  private Position position() {
    try {
      // Enable empty buttons.
      EventQueue.invokeLater(() -> buttons.values().stream().filter(e -> e.getIcon() == null).forEach(e -> e.setEnabled(true)));
      try {
        // Wait for user's input.
        Object position = positionQueue.take();
        // Return position. If it's not a position, quit...
        return position instanceof Position ? (Position) position : null;
      } catch (InterruptedException e) {
        // Interrupted while waiting for user's input... Quit...
        return null;
      }
    } finally {
      // Disable all buttons.
      EventQueue.invokeLater(() -> buttons.values().stream().forEach(e -> e.setEnabled(false)));
    }
  }

  @Override
  public void dispose() {

    // Drain.
    positionQueue.clear();
    // It's not allowed to offer null value :'-(
    positionQueue.offer(new Object());

    super.dispose();
  }

  public static void main(String[] args) {
    XOWindow.newInstanceEDT(Tag.O);
  }
}
