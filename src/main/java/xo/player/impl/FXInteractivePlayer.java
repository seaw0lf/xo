package xo.player.impl;

import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.StampedLock;

import automaton.StateAutomatonListener;
import javafx.application.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import xo.competition.Score;
import xo.match.*;
import xo.match.Grid.Position;
import xo.player.*;

/**
 * JavaFX/GUI player.
 * <p>
 * Provides a lightweight GUI with a grid and a score board.
 * 
 * @see Player
 *
 */
public final class FXInteractivePlayer extends Application implements Player {

  /**
   * This player's tag.
   */
  private static Tag TAG = Tag.X;

  /**
   * Lock stamp.
   */
  private static long controllerWriteLockStamp;
  /**
   * Stamped lock.
   * Locks the interactive player during the JavaFX framework and FXWindow initialization. 
   */
  private static StampedLock controllerLock = new StampedLock();

  /**
   * FXWindow controller.
   */
  private static FXWindowController controller;

  /**
   * Returns unlocked FXWindow controller.
   * @return FXWindow controller
   */
  private FXWindowController controller() {
    long stamp = controllerLock.readLock();
    try {
      return controller;
    } finally {
      controllerLock.unlockRead(stamp);
    }
  }

  @Override
  public void init(Tag tag) throws Exception {
    FXInteractivePlayer.TAG = Objects.requireNonNull(tag);

    // Lock the controller.
    controllerWriteLockStamp = controllerLock.writeLock();
    // Launch the JavaFX application in a separate thread.
    Executors.newSingleThreadExecutor().submit(() -> launch());
  }

  @Override
  public void start(Stage primaryStage) throws Exception {

    FXMLLoader xoLoader = new FXMLLoader(getClass().getClassLoader().getResource("xo.fxml"));

    // Prepare stage for the show.
    primaryStage.setScene(new Scene(xoLoader.load()));
    primaryStage.setTitle("Tic-Tac-Toe (" + TAG + ")");
    primaryStage.getIcons().add(new Image("tag/" + TAG + ".png"));
    primaryStage.setResizable(false);
    primaryStage.centerOnScreen();
    primaryStage.show();

    // Get the controller reference.
    controller = xoLoader.getController();
    controller.tag(TAG);

    // Close controller on stage close request.
    primaryStage.setOnCloseRequest(event -> controller.close());

    // Now controller is initialized. Thus, it can be unlocked.
    if (controllerLock.isWriteLocked()) {
      controllerLock.unlockWrite(controllerWriteLockStamp);
    }
  }

  @Override
  public Position apply(Board board) {
    return controller().positionDispatched(board);
  }

  @Override
  public StateAutomatonListener<Board> boardListener() {
    return new StateAutomatonListener<Board>() {

      @Override
      public void automatonStopped(Board finalState) {
        // Show the match outcome for the player.
        switch (finalState.getStatus()) {
          case DRAW:
            controller().outcomeDialogDispatched(FXWindowController.Outcome.DRAW, "Draw", finalState);
            break;
          case WIN:
            final boolean won = finalState.getOnTurn() == TAG.next();
            controller().outcomeDialogDispatched(won ? FXWindowController.Outcome.WIN : FXWindowController.Outcome.LOSS, won ? "Win" : "Loss", finalState);
            break;
          default:
            break;
        }
      }

      @Override
      public void automatonTerminated(Board state, Throwable cause) {
        // Show message if the other player quits.
        if (state.getOnTurn() != TAG && cause instanceof QuitException) {
          controller().messageDialogDispatched(FXWindowController.Outcome.QUIT, "Opponent quits...");
        }
      }
    };
  }

  @Override
  public StateAutomatonListener<Score> scoreListener() {
    return new StateAutomatonListener<Score>() {
      @Override
      public void afterStateChange(Score state) {
        // Update score after each match.
        controller().updateScoreDispatched(state);
      }
    };
  }

  @Override
  public void close() {
    Platform.exit();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
