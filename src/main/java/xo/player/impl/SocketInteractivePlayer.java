package xo.player.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple remote console player.
 * Delegates console IO to a port (X: 51966 and O: 51729).
 * <p>
 * Use telnet to connect to a Tic-Tac-Toe competition/match remotely.
 * 
 * @see ConsoleInteractivePlayer
 *
 */
public final class SocketInteractivePlayer extends ConsoleInteractivePlayer {
  private static final Logger log = Logger.getLogger(SocketInteractivePlayer.class.getName());

  /**
   * Server socket.
   */
  private ServerSocket serverSocket;

  /**
   * Client socket.
   */
  private Socket socket;

  @Override
  protected void initIO() throws IOException {

    // X - port: 51966
    // O - port: 51729
    int port = 0xCAFE - 0xED * tag.index();
    log.info(tag + ": port " + port);

    serverSocket = new ServerSocket(port);
    socket = serverSocket.accept();

    log.fine(tag + ": port " + port + ": connected");

    in = new Scanner(socket.getInputStream());
    out = new PrintWriter(socket.getOutputStream(), true);

    out.println("Tic-Tac-Toe");
    out.println("Remote Console Client");
    out.println("Player: " + tag);
  }

  @Override
  public void close() {
    super.close();

    if (socket != null) {
      try {
        socket.close();
      } catch (IOException e) {
        log.log(Level.WARNING, "Error closing socket", e);
      }
    }
    if (serverSocket != null) {
      try {
        serverSocket.close();
      } catch (IOException e) {
        log.log(Level.WARNING, "Error closing server socket", e);
      }
    }
  }
}
