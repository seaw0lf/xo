package xo.player.impl;

import java.util.*;
import java.util.concurrent.*;

import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.stage.StageStyle;
import xo.competition.Score;
import xo.match.*;
import xo.match.Grid.Position;
import xo.player.Tag;

/**
 * FXWindow controller.
 * Controls behavior of the scoreboard and the grid.
 *
 */
public class FXWindowController {

  /**
   * Corresponds to a {@link Grid.Position position} enum element.
   */
  @FXML
  private Button NW, N, NE, W, C, E, SW, S, SE;

  /**
   * Players score.
   */
  @FXML
  private Label scoreX, scoreO;

  /**
   * Players tag (image).
   */
  @FXML
  private ImageView playerX, playerO;

  /**
   * Button bindings container.
   * The only purpose is to hold strong references to the bindings.
   */
  private List<BooleanBinding> buttonBindings = new ArrayList<>();

  /**
   * Match outcome.
   */
  enum Outcome {
    WIN, DRAW, LOSS, QUIT;
  }

  /** Current player's tag. */
  private Tag tag;
  /** Player-on-turn indicator. */
  private Map<Tag, ImageView> players = new EnumMap<>(Tag.class);
  /** Score indicator. */
  private Map<Tag, Label> scores = new EnumMap<>(Tag.class);
  /** Buttons. One for each position. */
  private Map<Position, Button> buttons = new EnumMap<>(Position.class);

  /**
   * Position queue consumer thread.
   */
  private Thread positionQueueThread;
  /**
   * An easy-to-use 'pipe' to communicate between main-XO and JavaFX thread.
   */
  private BlockingQueue<Position> positionQueue = new ArrayBlockingQueue<>(1);

  /**
   * Populates FXML nodes into collections.
   */
  @FXML
  protected void initialize() {

    buttons.put(Position.NW, NW);
    buttons.put(Position.N, N);
    buttons.put(Position.NE, NE);
    buttons.put(Position.W, W);
    buttons.put(Position.C, C);
    buttons.put(Position.E, E);
    buttons.put(Position.SW, SW);
    buttons.put(Position.S, S);
    buttons.put(Position.SE, SE);

    scores.put(Tag.X, scoreX);
    scores.put(Tag.O, scoreO);

    players.put(Tag.X, playerX);
    players.put(Tag.O, playerO);

    Map<Tag, PseudoClass> buttonsPseudoClass = new EnumMap<>(Tag.class);
    buttonsPseudoClass.put(Tag.X, PseudoClass.getPseudoClass("X"));
    buttonsPseudoClass.put(Tag.O, PseudoClass.getPseudoClass("O"));

    // Bind the button text property to the pseudo class X/O.
    Platform.runLater(() -> {
      for (Button button : buttons.values()) {
        for (Tag t : Tag.values()) {
          // Buttons text property to tag value binding.
          BooleanBinding binding = button.textProperty().isEqualTo(t.toString());
          // Notify the pseudo class that the text property changed from/to tag value.
          binding.addListener((observable, oldValue, newValue) -> button.pseudoClassStateChanged(buttonsPseudoClass.get(t), newValue));
          // A strong reference to the bindings. We don't want them to be GC-ed.
          buttonBindings.add(binding);
        }
      }
    });
  }

  /**
   * Sets this players tag.
   * @param tag Player tag
   */
  void tag(Tag tag) {
    this.tag = tag;
  }

  /**
   * Disables and tags the grid position.
   * Adds the position to the position queue (input for the player).
   * @param event Button click event
   */
  @FXML
  void buttonClick(ActionEvent event) {
    Button button = (Button) event.getSource();
    button.setDisable(true);
    positionQueue.add(Position.valueOf(button.getId()));
    button.setText(tag.toString());
  }

  /**
   * Clears the grid.
   */
  private void clearGrid() {
    // Remove icon & disable each button.
    buttons.values().stream().forEach(button -> {
      button.setDisable(true);
      button.setText(null);
    });
  }

  /**
   * Updates grid according to the match board.
   * @param board Current board
   */
  private void updateGrid(Board board) {
    // Update each button's icon according to the match board.
    buttons.entrySet().stream().forEach(entry -> {
      Tag t = board.tag(entry.getKey());
      entry.getValue().setText(t == null ? null : t.toString());
    });
  }

  /**
   * Updates score indicator according to the competition score.
   * @param score Current score
   */
  private void updateScore(Score score) {
    // Update each player's score according to the competition score.
    scores.entrySet().stream().forEach(e -> e.getValue().setText(String.valueOf(score.getScore(e.getKey()))));
  }

  /**
   * Updates player-on-turn indicator according to the tag.
   * @param tag Tag
   */
  private void updateOnTurn(Tag tag) {
    // Highlight the player with given tag.
    players.entrySet().stream().forEach(e -> e.getValue().setDisable(e.getKey() != tag));
  }

  /**
   * Highlights the winning row of the current match.
   * @param finalState Final board of a match
   */
  private void highlightWin(Board finalState) {
    final Tag winner = finalState.getOnTurn().next();
    // From each possible row...
    Position.ROWS.stream()
        // ... retain those constituted of winner's tag only...
        .filter(row -> row.stream().map(p -> finalState.tag(p)).allMatch(tag -> tag == winner))
        // ... and highlight them.
        .forEach(row -> row.stream().forEach(pos -> buttons.get(pos).setDisable(false)));
  }

  /**
   * Waits for user's input.
   * @return Position to be tagged
   */
  private Position position() {
    positionQueueThread = Thread.currentThread();
    try {
      // Enable empty buttons.
      Platform.runLater(() -> buttons.values().stream().filter(button -> button.getText() == null).forEach(e -> e.setDisable(false)));
      try {
        // Wait for user's input & return position.
        return positionQueue.take();
      } catch (InterruptedException e) {
        // Interrupted while waiting for user's input... Quit...
        return null;
      }
    } finally {
      // Disable all buttons.
      Platform.runLater(() -> buttons.values().stream().forEach(e -> e.setDisable(true)));
    }
  }

  /**
   * Updates player score.
   * @param score Current player score
   */
  void updateScoreDispatched(Score score) {
    Platform.runLater(() -> updateScore(score));
  }

  /**
   * Updates grid, player on turn, and waits for user's input.
   * @param board Current board
   * @return Player's move, i.e. position to be tagged
   * @see #position()
   */
  Position positionDispatched(Board board) {
    Platform.runLater(() -> updateOnTurn(tag));
    try {
      Platform.runLater(() -> updateGrid(board));
      return position();
    } finally {
      Platform.runLater(() -> updateOnTurn(tag.next()));
    }
  }

  /**
   * Shows game outcome. Then cleans the grid.
   * @param outcome Game outcome
   * @param message Dialog message
   * @param finalState Current (final) board
   */
  void outcomeDialogDispatched(Outcome outcome, String message, Board finalState) {

    Platform.runLater(() -> updateGrid(finalState));

    if (outcome == Outcome.WIN || outcome == Outcome.LOSS) {
      Platform.runLater(() -> highlightWin(finalState));
    }

    messageDialogDispatched(outcome, message);

    Platform.runLater(() -> clearGrid());
  }

  /**
   * Shows message dialog.
   * @param outcome Game outcome
   * @param message Dialog message
   */
  void messageDialogDispatched(Outcome outcome, String message) {

    final CountDownLatch latch = new CountDownLatch(1);

    Platform.runLater(() -> {

      final Alert alert = new Alert(AlertType.NONE);

      alert.initStyle(StageStyle.UTILITY);
      alert.setHeaderText(null);
      alert.setResizable(false);

      final ImageView graphic = new ImageView("outcome/" + outcome.toString().toLowerCase() + ".png");
      alert.setGraphic(graphic);

      final DialogPane alertDialog = alert.getDialogPane();

      alertDialog.setMaxHeight(graphic.getFitHeight());
      alertDialog.setMaxWidth(graphic.getFitWidth());

      alertDialog.setOnMouseClicked(event -> {
        alert.setResult(ButtonType.CLOSE);
        alert.close();
      });
      alertDialog.getScene().getWindow().setOnCloseRequest(null);

      alert.showAndWait();

      latch.countDown();
    });

    try {
      latch.await();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Interrupts the position queue consumption.
   */
  void close() {
    if (positionQueueThread != null) {
      positionQueueThread.interrupt();
    }
  }
}
