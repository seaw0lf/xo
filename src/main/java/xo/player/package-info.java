/**
 * Provides interface that enables player implementation which can be used in a competition or a match
 */
package xo.player;
