package xo.competition;

import java.util.*;
import java.util.logging.*;

import automaton.StateAutomaton;
import xo.player.*;

/**
 * Player manager.
 * This class is responsible for
 * {@link #register(Map) registering} ({@link Player#forName(String, Tag) instantiation} and tag association) and
 * {@link #close() unregistering} players.
 * 
 * @see AutoCloseable
 * @see Iterable
 *
 */
public final class Players implements AutoCloseable, Iterable<Player> {
  private static final Logger log = Logger.getLogger(Players.class.getName());

  /**
   * Contains {@link Player player implementation} per each {@link Tag tag}.
   */
  private Map<Tag, Player> player;

  /**
   * Player manager constructor.
   */
  private Players() {
    log.fine(() -> Arrays.toString(new Object[] {}));
    player = new java.util.EnumMap<>(Tag.class);
  }

  /**
   * @param playerClassName Qualified class name per each tag
   * @return Player manager with instantiated players
   * @see #register(Map)
   * @see #close()
   */
  public static Players forName(Map<Tag, String> playerClassName) {
    Players players = new Players();
    try {
      players.register(playerClassName);
    } catch (Throwable t) {
      try {
        players.close();
      } catch (Throwable tClose) {
        t.addSuppressed(tClose);
      }
      throw t;
    }
    return players;
  }

  /**
   * Provides {@link Player player implementation} associated with the given {@link Tag tag}.
   * @param tag Player tag
   * @return Player implementation
   * @throws NullPointerException if tag is {@code null}
   */
  public Player get(Tag tag) {
    return player.get(Objects.requireNonNull(tag));
  }

  @Override
  public Iterator<Player> iterator() {
    return player.values().iterator();
  }

  /**
   * {@link #Players() Constructs} and {@link #register(Map) registers} players according to their qualified class name.
   * Registration of each player is performed in parallel.
   * @param playerClassName Qualified class name per each tag
   * @throws IllegalStateException on unsuccessful player instantiation
   */
  private void register(Map<Tag, String> playerClassName) {
    log.config(() -> Arrays.toString(new Object[] { playerClassName }));

    // Register players in parallel.
    Arrays.stream(Tag.values()).parallel().forEach(tag -> {
      try {
        log.fine("Player " + tag + ": registering...");
        Player instance = Player.forName(playerClassName.get(tag), tag);
        player.put(tag, instance);
        log.fine("Player " + tag + ": registered " + instance);
      } catch (Throwable t) {
        log.log(Level.SEVERE, "Player " + tag + ": registration failed", t);
      }
    });

    // Check if all players are registered.
    for (Tag tag : Tag.values()) {
      if (player.get(tag) == null)
        throw new IllegalStateException("Unable to instantiate player " + tag + ".");
    }

    log.fine("Players " + this + " registered");
  }

  @Override
  public void close() {
    log.fine(() -> Arrays.toString(new Object[] {}));

    // Close player resources in parallel.
    player.values().parallelStream().forEach(player -> {
      if (player != null) {
        try {
          player.close();
        } catch (Throwable t) {
          log.log(Level.WARNING, "Error occured while unregistering player " + player, t);
        }
      }
    });
  }

  @Override
  public String toString() {
    return String.valueOf(player);
  }

  /**
   * Extracts qualified class name per each {@link Tag player} from command-line arguments.
   * <b>Terminates the currently running JVM in case of incorrect number of arguments.</b>
   * @param args Command-line arguments
   * @param forClass Class for which are the command-line arguments processed
   * @return Qualified class name per each tag
   * @see System#exit(int)
   */
  public static Map<Tag, String> className(String[] args, Class<? extends StateAutomaton<?, ?>> forClass) {

    if (args.length != Tag.values().length) {
      System.out.println("Tic-Tac-Toe");
      System.out.println("Usage: " + forClass.getName() + " player_X_classname player_O_classname");
      System.out.println("\"Strange game. The only winning move is not to play.\" - Joshua, WarGames");
      System.out.println();
      System.out.println("  player_X_classname\txo.player.impl.ConsoleInteractivePlayer");
      System.out.println("  player_O_classname\txo.player.impl.SocketInteractivePlayer");
      System.out.println("                    \txo.player.impl.RandomAutonomousPlayer");
      System.exit(1);
    }

    // Extract player implementation from command line arguments.
    Map<Tag, String> playerClassName = new java.util.EnumMap<>(Tag.class);
    for (Tag tag : Tag.values()) {
      playerClassName.put(tag, args[tag.index()]);
    }

    return playerClassName;
  }
}
