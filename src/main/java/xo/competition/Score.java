package xo.competition;

import java.util.*;

import automaton.*;
import xo.match.*;
import xo.player.*;

/**
 * Immutable class representing {@link State state} of the {@link Competition competition automaton}.
 * Contains player score and an identification of the {@link Tag first player on turn}.
 * 
 * @see Cloneable
 * @see State
 *
 */
public final class Score implements Cloneable, State {

  /**
   * {@link Tag} of the player whose turn is expected to be the first in the next {@link Match match}.
   */
  private final Tag onTurn;
  
  /**
   * Contains score per each tag.
   */
  private final EnumMap<Tag, Integer> score;

  /**
   * Constructs a new score.
   * @param score Tic-Tac-Toe score
   * @param onTurn Player on first turn in the next match
   * @throws NullPointerException if score or tag is {@code null}
   */
  private Score(EnumMap<Tag, Integer> score, Tag onTurn) {
    this.score = Objects.requireNonNull(score);
    this.onTurn = Objects.requireNonNull(onTurn);
  }

  /**
   * First player on turn.
   * @return Tag of the first player on turn in the next match
   */
  public Tag getOnTurn() {
    return onTurn;
  }

  /**
   * Score of the given player.
   * @param tag Player tag
   * @return Score of the given player
   * @throws NullPointerException if tag is {@code null}
   */
  public int getScore(Tag tag) {
    return score.get(Objects.requireNonNull(tag));
  }

  @Override
  public boolean isFinal() {
    // Players can have a ball forever...
    return false;
  }

  @Override
  protected Score clone() {
    return new Score(score.clone(), onTurn);
  }

  @Override
  public String toString() {
    return score + ":" + onTurn;
  }

  /**
   * {@link Score Score objects} producer.
   * It is a {@link State.Factory state factory} since it can produce start state.
   * It is also a {@link TransitionFunction transition function} since it can produce new score configuration from a current score configuration and a {@link Tag match winner}.
   * 
   * @see State.Factory
   * @see TransitionFunction
   *
   */
  public static final class Factory implements State.Factory<Score>, TransitionFunction<Score, Tag> {

    /**
     * {@link Tag Player tag} for the {@link Score start state}.
     */
    private Tag startTag;

    /**
     * Score factory constructor.
     * @param startTag Player tag for the start state (player who starts the match)
     * @throws NullPointerException if tag is {@code null}
     */
    public Factory(Tag startTag) {
      this.startTag = Objects.requireNonNull(startTag);
    }

    @Override
    public Score startState() {

      // Each player starts with score of zero.
      EnumMap<Tag, Integer> score = new java.util.EnumMap<>(Tag.class);
      for (Tag tag : Tag.values()) {
        score.put(tag, 0);
      }

      return new Score(score, startTag);
    }

    @Override
    public Score apply(Score score, Tag winner) {
      Objects.requireNonNull(score, "Unknown score (state)");

      if (winner == null) {
        // DRAW
        // Player on turn is the one who started the draw match.
        // Let's give the other player a chance too!
        return new Score(score.score, score.onTurn.next());
      }

      // WIN (for the current player).
      // Increment the winner's score.
      EnumMap<Tag, Integer> scoreClone = score.score.clone();
      scoreClone.put(winner, scoreClone.get(winner) + 1);
      // Let's give the loser a chance too!
      return new Score(scoreClone, winner.next());
    }
  }
}
