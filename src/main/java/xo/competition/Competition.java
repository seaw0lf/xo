package xo.competition;

// Quotes:
//   "Perfection [in design] is achieved, not when there is nothing more to add, but when there is nothing left to take away." - Antoine de Saint-Exupery
//   "In labouring to be concise, I become obscure." - Quintus Horatius Flaccus

import java.util.*;
import java.util.logging.Logger;

import automaton.StateAutomaton;
import xo.match.*;
import xo.match.Board.Status;
import xo.player.*;

/**
 * Represents one Tic-Tac-Toe competition of {@link Players two competitors}.
 * <p>
 * Competition is a {@link StateAutomaton state automaton}:
 * <ul>
 * <li><i>&Sigma;</i> = all possible {@link Tag match winners}</li>
 * <li><i>Q</i> = all possible {@link Score score configurations}</li>
 * <li><i>&delta;</i> = a {@link Score.Factory score factory} for producing new score configuration from {@link Score current score configuration} and a {@link Tag match winner}</li>
 * <li><i>q<sub>0</sub></i> = {@link Score.Factory#startState() initial score} with player {@link Tag#X X} on turn</li>
 * <li><i>F</i> = &empty;</li>
 * </ul>
 *
 */
public final class Competition extends StateAutomaton<Score, Tag> {
  private static final Logger log = Logger.getLogger(Competition.class.getName());

  /**
   * The score factory.
   */
  private final Score.Factory factory;

  /**
   * Competition constructor.
   * @param players Competitors
   * @param first Tag of the first player on turn
   * @throws NullPointerException if players or tag is {@code null}
   */
  Competition(Players players, Tag first) {
    log.fine(() -> Arrays.toString(new Object[] { players, first }));

    this.factory = new Score.Factory(first);

    // Match winner is the Competition state automaton input.
    this.inputSupplier = (score) -> {
      try (Match match = new Match(players, score.getOnTurn())) {
        // Start match.
        match.start();
        // Return the winner tag or null (draw).
        Board finalState = match.getState();
        return finalState.getStatus() == Status.DRAW ? null : finalState.getOnTurn().next();
      }
    };

    for (Player player : players) {
      this.addListener(player.scoreListener());
    }
  }

  @Override
  public Score getState() {
    // Defensive copy
    return super.getState().clone();
  }

  @Override
  protected void open() {
    log.fine(() -> Arrays.toString(new Object[] {}));

    transition = factory;
    state = factory.startState();
  }

  @Override
  public void close() {
    log.fine(() -> Arrays.toString(new Object[] {}));
    removeListeners();
  }

  public static void main(String[] args) {

    Map<Tag, String> playerClassName = Players.className(args, Competition.class);

    // Start the competition. Player X shall be the first.
    try (
        Players competitors = Players.forName(playerClassName);
        Competition competition = new Competition(competitors, Tag.X)
    ) {
      try {
        competition.start();
      } finally {
        log.info(() -> "Score: " + competition.getState());
      }
    } catch (QuitException e) {
      log.info(() -> e.getMessage());
    }
  }
}
