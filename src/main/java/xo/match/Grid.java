package xo.match;

import java.util.*;

import automaton.Symbol;
import xo.player.Tag;

/**
 * Immutable class representing the Tic-Tac-Toe grid. Contains {@link Tag player tags}.
 * 
 * @see Cloneable
 *
 */
public final class Grid implements Cloneable {

  /**
   * Internal representation of the grid. Contains {@link Tag tag} per each {@link Position position}.
   */
  private final EnumMap<Position, Tag> grid;

  /**
   * Lazy init hash code cache.
   */
  private int hash;

  /**
   * Grid constructor.
   * @param grid Internal grid representation
   * @throws NullPointerException if grid is {@code null}
   */
  private Grid(EnumMap<Position, Tag> grid) {
    this.grid = Objects.requireNonNull(grid);
  }

  /**
   * Empty grid constructor.
   */
  public Grid() {
    this(new EnumMap<>(Position.class));
  }

  /**
   * Determines number of empty {@link grid} {@link Position positions}.
   * @return Number of empty positions in the grid
   */
  public int emptyPositions() {
    int empty = Position.values().length;
    for (Tag tag : grid.values()) {
      if (tag != null) {
        empty--;
      }
    }
    return empty;
  }

  /**
   * {@link Tag} at the given {@link Position position}.
   * @param position Grid position
   * @return Tag at the given position
   * @throws NullPointerException if position is {@code null}
   */
  public Tag tag(Position position) {
    return grid.get(Objects.requireNonNull(position));
  }

  /**
   * {@link Tag Tags} the given {@link Position position}.
   * @param position Grid position
   * @param tag Tag to use
   * @return New grid instance with tagged position
   * @throws NullPointerException if position or tag is {@code null}
   */
  public Grid tag(Position position, Tag tag) {
    Grid newGrid = clone();
    newGrid.grid.put(position, Objects.requireNonNull(tag));
    return newGrid;
  }

  /**
   * Determines if a {@link Tag player} owns all positions in the given {@link Position#ROWS row}.
   * @param row Row, column or a diagonal
   * @return {@code true} iff a player owns all positions in the given {@code row}
   * @throws NullPointerException if row is {@code null}
   */
  public Tag allInRow(List<Position> row) {

    Tag tag = grid.get(row.get(0));
    for (int i = 1; i < row.size(); i++) {
      if (grid.get(row.get(i)) != tag)
        return null;
    }
    return tag;
  }

  @Override
  protected Grid clone() {
    return new Grid(grid.clone());
  }

  @Override
  public int hashCode() {
    if (hash == 0 && !grid.isEmpty()) {
      return hash = grid.hashCode();
    }
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    return grid.equals(((Grid) obj).grid);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (List<Position> row : Arrays.asList(Position.ROWS.get(0), Position.ROWS.get(1), Position.ROWS.get(2))) {
      for (Position col : row) {
        builder.append(Objects.toString(grid.get(col), ".")).append(' ');
      }
      builder.append(System.lineSeparator());
    }
    return builder.toString();
  }

  /**
   * Represents position in a {@link Grid grid}:
   * <pre>
   * NW | N | NE
   * ---+---+---
   *  W | C |  E
   * ---+---+---
   * SW | S | SE</pre>
   * Position is a {@link Match match automaton} {@link Symbol input symbol}.
   * 
   * @see Symbol
   *
   */
  public enum Position implements Symbol {
    NW, N, NE,
     W, C,  E,
    SW, S, SE;

    /**
     * List of all possible <i>winning positions</i>.
     * <p>
     * <i>Winning position</i> is an array of positions representing row, column or diagonal of a grid.
     */
    public static final List<List<Position>> ROWS;

    static {

      List<List<Position>> rows = new ArrayList<>(8);
      rows.add(Collections.unmodifiableList(Arrays.asList(NW, N, NE)));
      rows.add(Collections.unmodifiableList(Arrays.asList(W, C,  E)));
      rows.add(Collections.unmodifiableList(Arrays.asList(SW, S, SE)));
      rows.add(Collections.unmodifiableList(Arrays.asList(NW, W, SW)));
      rows.add(Collections.unmodifiableList(Arrays.asList(N, C,  S)));
      rows.add(Collections.unmodifiableList(Arrays.asList(NE, E, SE)));
      rows.add(Collections.unmodifiableList(Arrays.asList(NW, C, SE)));
      rows.add(Collections.unmodifiableList(Arrays.asList(SW, C, NE)));

      ROWS = Collections.unmodifiableList(rows);
    }
  }
}
