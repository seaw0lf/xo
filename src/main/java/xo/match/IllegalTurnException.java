package xo.match;

import automaton.UnsuccessfulStateChangeException;
import xo.match.Grid.Position;

/**
 * Signals that player performed an illegal turn.
 *
 */
public final class IllegalTurnException extends UnsuccessfulStateChangeException {
  private static final long serialVersionUID = 1;

  /**
   * Constructs an exception with the illegal {@link Position position} the player responded to the current {@link Board board}.
   * @param position Player's move
   * @param board Board the player should respond to
   */
  public IllegalTurnException(Position position, Board board) {
    super("Unable to mark position " + position + " on board:\n" + board);
  }
}
