package xo.match;

import automaton.TerminatedStateAutomatonException;
import xo.player.Tag;

/**
 * Signals that player wants to quit a match/competition.
 * <p>
 * {@link QuitException} must be a subclass of {@link TerminatedStateAutomatonException} since match/competition cannot continue without any player.
 *
 */
public final class QuitException extends TerminatedStateAutomatonException {
  private static final long serialVersionUID = 1;

  /**
   * Quitter's tag.
   */
  private Tag quitter;

  /**
   * Constructs an exception with the quitter.
   * @param quitter Tag
   */
  public QuitException(Tag quitter) {
    super(quitter + " quits");
    this.quitter = quitter;
  }

  /**
   * Returns tag of the quitting player.
   * @return Tag Quitter
   */
  protected Tag getQuitter() {
    return quitter;
  }
}
