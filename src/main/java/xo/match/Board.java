package xo.match;

import java.util.*;

import automaton.*;
import xo.match.Grid.Position;
import xo.player.Tag;

/**
 * Immutable class representing {@link State state} of the {@link Match match automaton}.
 * Contains the {@link Grid grid} and an identification of the {@link Tag player on turn}.
 *
 * @see Cloneable
 * @see State
 *
 */
public final class Board implements Cloneable, State {

  /**
   * Represents board status.
   */
  public enum Status {
    /** Match in progress. */
    PLAYING,
    /** Draw match. */
    DRAW,
    /** Winning match. */
    WIN;

    /**
     * Determines {@link Status status} of the given {@link Board board}.
     * @param board Board to examine
     * @return Status of the examined board
     */
    public static Status of(Board board) {
      for (List<Position> row : Position.ROWS) {
        if (board.grid.allInRow(row) != null) {
          return Status.WIN;
        }
      }
      return board.emptyPositions() == 0 ? Status.DRAW : Status.PLAYING;
    }
  }

  /**
   * The {@link Grid grid}.
   */
  private final Grid grid;

  /**
   * {@link Tag} of the player whose turn is expected.
   */
  private final Tag onTurn;

  /**
   * Lazy init {@link Status status} cache.
   */
  private Status status;

  /**
   * Board constructor.
   * @param grid Tic-Tac-Toe grid
   * @param onTurn Player on turn
   * @throws NullPointerException if grid or tag is {@code null}
   */
  private Board(Grid grid, Tag onTurn) {
    this.grid = Objects.requireNonNull(grid);
    this.onTurn = Objects.requireNonNull(onTurn);
  }

  /**
   * Determines number of empty {@link Grid#emptyPositions() grid positions}.
   * @return Number of empty positions in the grid
   */
  public int emptyPositions() {
    return grid.emptyPositions();
  }

  @Override
  public boolean isFinal() {
    return getStatus() != Status.PLAYING;
  }

  /**
   * {@link #grid Grid} of this {@link Board board}.
   * @return Defensive copy of the grid
   */
  public Grid getGrid() {
    return grid.clone();
  }

  /**
   * Player on turn.
   * @return Tag of the player on turn
   */
  public Tag getOnTurn() {
    return onTurn;
  }

  /**
   * Board status.
   * @return Status of this board
   */
  public Status getStatus() {
    if (status == null) {
      return status = Status.of(this);
    }
    return status;
  }

  /**
   * {@link Tag} at the given {@link Position position}.
   * @param position Grid position
   * @return Tag at the given position
   * @throws NullPointerException if position is {@code null}
   */
  public Tag tag(Position position) {
    return grid.tag(position);
  }

  @Override
  protected Board clone() {
    return new Board(grid.clone(), onTurn);
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    // Current player's tag.
    builder.append("On turn: ").append(onTurn).append(System.lineSeparator());
    // Grid.
    builder.append(grid);

    return builder.toString();
  }

  /**
   * {@link Board Board objects} producer.
   * It is a {@link State.Factory state factory} since it can produce start state.
   * It is also a {@link TransitionFunction transition function} since it can produce new board configuration from a current board configuration and a {@link Position position} which took the player on turn.
   * 
   * @see State.Factory
   * @see TransitionFunction
   *
   */
  public static final class Factory implements State.Factory<Board>, TransitionFunction<Board, Position> {

    /**
     * {@link Tag Player tag} for the {@link Board start state}.
     */
    private Tag startTag;

    /**
     * Board factory constructor.
     * @param startTag Player tag for the start state (player who starts the match)
     * @throws NullPointerException if tag is {@code null}
     */
    public Factory(Tag startTag) {
      this.startTag = Objects.requireNonNull(startTag);
    }

    @Override
    public Board startState() {
      return new Board(new Grid(), startTag);
    }

    @Override
    public Board apply(Board board, Position position) {
      Objects.requireNonNull(board, "Unknown board (state)");
      Objects.requireNonNull(position, "Unknown position (transition)");

      // Adding a tag is not supported on a final board (winner is known, no need to continue).
      if (board.isFinal())
        throw new UnsupportedOperationException("Board is final");
      // Rewriting a tag in a grid is unacceptable.
      if (board.grid.tag(position) != null)
        throw new IllegalTurnException(position, board);

      return new Board(board.grid.tag(position, board.onTurn), board.onTurn.next());
    }
  }
}
