package xo.match;

import java.util.*;
import java.util.logging.Logger;

import automaton.finite.FiniteStateAutomaton;
import xo.competition.Players;
import xo.match.Grid.Position;
import xo.player.*;

/**
 * Represents one Tic-Tac-Toe match of {@link Players two competitors}.
 * <p>
 * Match is a {@link FiniteStateAutomaton finite state automaton} (since there are only 3*3 positions in a grid):
 * <ul>
 * <li><i>&Sigma;</i> = all possible {@link Position positions in the grid}</li>
 * <li><i>Q</i> = all possible {@link Board board configurations}</li>
 * <li><i>&delta;</i> = a {@link Board.Factory board factory} for producing new board configuration from current {@link Board board configuration} and a {@link Position position}</li>
 * <li><i>q<sub>0</sub></i> = {@link Board.Factory#startState() board} with an {@link Grid#Grid() empty grid} and an appropriate {@link Tag player on turn}</li>
 * <li><i>F</i> = subset of all board configurations for which {@link Board#isFinal() board is final}</li>
 * </ul>
 *
 * @see FiniteStateAutomaton
 *
 */
public final class Match extends FiniteStateAutomaton<Board, Position> {
  private static final Logger log = Logger.getLogger(Match.class.getName());

  /**
   * The board factory.
   */
  private final Board.Factory factory;

  /**
   * Match constructor.
   * @param players Players
   * @param first Tag of the first player on turn
   * @throws NullPointerException if players or tag is {@code null}
   */
  public Match(Players players, Tag first) {
    log.fine(() -> Arrays.toString(new Object[] { players, first }));

    this.factory = new Board.Factory(first);

    // Player's move (position which current player intends to tag) is the Match automaton input.
    this.inputSupplier = board -> {

      // Request next move from the player on turn based on defensive copy of the current board.
      Position position = players.get(board.getOnTurn()).apply(board.clone());
      // No position is interpreted as quit request.
      if (position == null)
        throw new QuitException(board.getOnTurn());

      return position;
    };

    for (Player player : players) {
      this.addListener(player.boardListener());
    }
  }

  @Override
  public Board getState() {
    // Defensive copy
    return super.getState().clone();
  }

  @Override
  protected void open() {
    log.fine(() -> Arrays.toString(new Object[] {}));

    transition = factory;
    state = factory.startState();

    // Limit the number of move attempts to a sane number.
    transitions(state.emptyPositions() * 3);
  }

  @Override
  public void close() {
    log.fine(() -> Arrays.toString(new Object[] {}));
    removeListeners();
  }

  public static void main(String[] args) {

    Map<Tag, String> playerClassName = Players.className(args, Match.class);

    // Play one match.
    try (
        Players players = Players.forName(playerClassName);
        Match match = new Match(players, Tag.X)
    ) {
      match.start();
    } catch (QuitException e) {
      log.info(() -> e.getMessage());
    }
  }
}
