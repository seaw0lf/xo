package automaton;

import java.util.function.Function;

/**
 * Represents the input string (<i>w</i> &isin; <i>&Sigma;*</i>) supplier.
 * Implementation of this interface is responsible for supplying the {@link StateAutomaton automaton} with an <i>input string</i>.
 * <p>
 * <i>Input string</i> is supplied as a sequence of individual symbols. Each {@link Symbol symbol} is supplied with
 * respect to the {@link State current state} of the automaton.
 * 
 * @param <Q> Current state
 * @param <S> Input symbol
 * 
 * @see Function
 * 
 */
public interface InputSupplier<Q extends State, S extends Symbol> extends Function<Q, S> {
}
