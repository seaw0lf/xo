package automaton;

import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * This class represents an observable object. It can be subclassed to represent an object that the application wants to
 * have observed.
 * <p>
 * An observable object can have one or more observers - listeners. An listener may be any object that implements
 * interface {@link EventListener}.
 * <p>
 * The order in which notifications will be delivered is unspecified and possibly parallel.
 * 
 * @param <L> Listener which observes.
 *
 */
public class Observable<L extends EventListener> {
  private static final Logger log = Logger.getLogger(Observable.class.getName());

  /**
   * Set of registered {@link EventListener listeners}.
   * <p>
   * Set is wrapped with a synchronized implementation to ensure thread-safe listener registration.
   */
  private Set<L> listeners = Collections.synchronizedSet(new HashSet<>());

  /**
   * Adds a {@link EventListener listener} to the set of listeners for this object, provided that it is not the same as some listener already
   * in the set. The order in which notifications will be delivered to multiple listeners is not specified.
   * 
   * @param listener The listener to be added
   */
  public void addListener(L listener) {
    log.finest(() -> String.valueOf(listener));

    if (listener == null)
      return;

    listeners.add(listener);
  }

  /**
   * Removes a {@link EventListener listener} from the set of listeners of this object. Passing {@code null} to this method will have no
   * effect.
   * 
   * @param listener The listener to be removed
   */
  public void removeListener(L listener) {
    log.finest(() -> String.valueOf(listener));
    listeners.remove(listener);
  }

  /**
   * Removes all {@link EventListener listeners} from the set of listeners of this object.
   */
  public void removeListeners() {
    log.finest(() -> String.valueOf("all"));
    listeners.clear();
  }

  /**
   * Notifies all {@link EventListener listeners} from the set of listeners of this object.
   * <p>
   * Each listener has its listening methods called using {@link Consumer notification consumer}.
   * <p>
   * The order in which notifications will be delivered is unspecified and possibly parallel.
   * 
   * @param notification Listening method to be notified
   */
  protected void notifyListeners(Consumer<L> notification) {
    log.finest(() -> listeners + " " + notification);
    // Wrapped as an ArrayList to notify in parallel.
    new ArrayList<>(listeners).parallelStream().forEach(notification);
  }
}
