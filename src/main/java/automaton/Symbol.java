package automaton;

/**
 * Symbol (<i>a</i> &isin; <i>&Sigma;</i>) of an {@link StateAutomaton automaton}.
 * 
 */
public interface Symbol {
}
