package automaton;

import java.util.EventListener;

/**
 * The listener interface for receiving {@link StateAutomaton automaton} events.
 * When an automaton event occurs, corresponding method is invoked.
 * <p>
 * Class interested in processing an automaton event should implement this interface and register it
 * with the automaton using the {@link Observable#addListener(EventListener) addListener} method.
 *
 * @param <Q> State
 * 
 * @see EventListener
 * 
 */
public interface StateAutomatonListener<Q extends State> extends EventListener {

  /**
   * Invoked when the automaton's run phase starts.
   * 
   * @param startState Start state
   */
  default void automatonStarted(Q startState) {
  }

  /**
   * Invoked when the automaton's run phase ends without an exception.
   * This and {@link #automatonTerminated(State, Throwable) automatonTerminated} event are invoked exclusively.
   * 
   * @param finalState Final state
   */
  default void automatonStopped(Q finalState) {
  }

  /**
   * Invoked when the automaton's run phase ends with an exception.
   * This and {@link #automatonStopped(State) automatonStopped} event are invoked exclusively.
   * 
   * @param state Current state (before state change)
   * @param cause Cause
   */
  default void automatonTerminated(Q state, Throwable cause) {
  }

  /**
   * Invoked before the automaton's state is changed.
   * 
   * @param state Current state
   */
  default void beforeStateChange(Q state) {
  }

  /**
   * Invoked after the automaton's state is changed.
   * This and {@link #unsuccessfulStateChange(State, Throwable) unsuccessfulStateChange} event are invoked exclusively.
   * 
   * @param state Current state (after state change)
   */
  default void afterStateChange(Q state) {
  }

  /**
   * Invoked when automaton's attempt to change the state ends with an exception.
   * This and {@link #afterStateChange(State) afterStateChange} event are invoked exclusively.
   * 
   * @param state Current state (before state change)
   * @param cause Cause
   */
  default void unsuccessfulStateChange(Q state, Throwable cause) {
  }
}
