package automaton.finite;

import automaton.TerminatedStateAutomatonException;

/**
 * Thrown if a deterministic finite state automaton exceeds a transition limit.
 *
 */
public class TransitionLimitExceededException extends TerminatedStateAutomatonException {
  private static final long serialVersionUID = 1;

  /**
   * Constructs a new {@code TransitionLimitExceededException} with {@code null} as its detail message. The cause
   * is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
   */
  public TransitionLimitExceededException() {
    super();
  }

  /**
   * Constructs a new {@code TransitionLimitExceededException} with the specified detail message. The cause is not
   * initialized, and may subsequently be initialized by a call to {@link #initCause}.
   *
   * @param message
   *          The detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
   */
  public TransitionLimitExceededException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@code TransitionLimitExceededException} with the specified detail message and cause.
   *
   * @param message
   *          The detail message (which is saved for later retrieval by the {@link #getMessage()} method)
   * @param cause
   *          The cause (which is saved for later retrieval by the {@link #getCause()} method). (A {@code null}
   *          value is permitted, and indicates that the cause is nonexistent or unknown.)
   */
  public TransitionLimitExceededException(String message, Throwable cause) {
    super(message, cause);
  }
}
