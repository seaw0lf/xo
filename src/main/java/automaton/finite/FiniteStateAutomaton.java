package automaton.finite;

import java.util.logging.Logger;

import automaton.*;

/**
 * Represents a <a href="https://en.wikipedia.org/wiki/Automata_theory#Formal_definition">deterministic finite state automaton</a>.
 * <p>
 * Finiteness of an automaton is enforced by a <i>limit</i>. {@link #limit Limit} represents the number of state transitions an automaton is
 * allowed to perform. Default value is 0, i.e. finite automaton will remain in start state and every attempt to change
 * its state will result in an {@link TransitionLimitExceededException transition limit termination exception}.
 * 
 * @param <Q> State
 * @param <S> Input symbol
 * 
 * @see StateAutomaton
 *
 */
public abstract class FiniteStateAutomaton<Q extends State, S extends Symbol> extends StateAutomaton<Q, S> {
  private static final Logger log = Logger.getLogger(FiniteStateAutomaton.class.getName());

  /**
   * Number of remaining {@link #changeState() state transitions}. Default is 0.
   */
  private int limit;

  /**
   * Specifies transition limit.
   * 
   * @param limit Transition limit
   * @throws IllegalArgumentException if limit is negative
   */
  protected void transitions(int limit) {
    log.finer(() -> String.valueOf(limit));

    if (limit < 0)
      throw new IllegalArgumentException();

    this.limit = limit;
  }

  /**
   * Prohibits state change if the limit is exceeded. Otherwise the state is changed using the superclass
   * implementation.
   * 
   * @throws TransitionLimitExceededException if number of state changes exceeds limit
   */
  @Override
  protected void changeState() throws TransitionLimitExceededException {

    if (limit-- == 0)
      throw new TransitionLimitExceededException();

    super.changeState();
  }
}
