/**
 * Provides classes and interfaces for a Deterministic Finite-state Automaton implementation
 */
package automaton.finite;
