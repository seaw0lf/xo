package automaton;

/**
 * State (<i>q</i> &isin; <i>Q</i>) of an {@link StateAutomaton automaton}.
 *
 */
@FunctionalInterface
public interface State {

  /**
   * Is this state final (<i>q</i> &isin; <i>F</i>)?
   * 
   * @return {@code true} iff <i>q</i> &isin; <i>F</i>
   */
  boolean isFinal();

  /**
   * Start state (<i>q<sub>0</sub></i> &isin; <i>Q</i>) factory.
   *
   * @param <S> State
   *
   */
  @FunctionalInterface
  public interface Factory<S extends State> {

    /**
     * Produces the start state.
     * 
     * @return The start state
     */
    S startState();
  }
}
