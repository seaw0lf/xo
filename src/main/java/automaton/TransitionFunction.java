package automaton;

import java.util.function.BiFunction;

/**
 * Represents state transition function (<i>&delta;</i>) of an {@link StateAutomaton automaton}.
 * Defines rules that tell how the {@link State state} changes with respect to {@link Symbol input}.
 *
 * @param <Q> Current state
 * @param <S> Input symbol
 *
 * @see BiFunction
 *
 */
public interface TransitionFunction<Q extends State, S extends Symbol> extends BiFunction<Q, S, Q> {
}
