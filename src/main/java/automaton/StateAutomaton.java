package automaton;

import java.util.logging.Logger;

/**
 * Represents a <a href="https://en.wikipedia.org/wiki/Automata_theory#Formal_definition">deterministic state automaton</a> (<i>Q</i>, <i>&Sigma;</i>, <i>&delta;</i>, <i>q<sub>0</sub></i>, <i>F</i>).
 * <p>
 * Automaton changes its {@link State state} using {@link TransitionFunction transition function} which is supplied with
 * {@link Symbol input symbols} by an {@link InputSupplier input supplier}.
 * <p>
 * A typical invocation sequence that creates and starts an automaton is:
 * <pre>
 * try (AStateAutomaton&lt;AState, ASymbol&gt; dsa = new AStateAutomaton()) {
 *   dsm.start();
 * }</pre>
 * <p>
 * A typical life-cycle of an automaton is:
 * <ol>
 *   <li>instantiated</li>
 *   <li>{@link #start() started}:
 *     <ol type="a">
 *       <li>{@link #open() opened} - subclass should instantiate {@link State start state}, {@link TransitionFunction transition function}, and {@link InputSupplier input supplier}</li>
 *       <li>{@link #run() running} - changes state using transition function supplied by input supplier</li>
 *     </ol>
 *   </li>
 *   <li>stopped - due to {@link State#isFinal() final state} or {@link TerminatedStateAutomatonException terminated exception}</li>
 *   <li>{@link AutoCloseable#close() closed} </li>
 * </ol>
 * 
 * @see "Section 2.2.1 in Hopcroft, Motwani, Ullman - Introduction to Automata Theory, Languages, and Computation"
 * 
 * @param <Q> State
 * @param <S> Input symbol
 *
 */
public abstract class StateAutomaton<Q extends State, S extends Symbol> extends Observable<StateAutomatonListener<Q>> implements AutoCloseable {
  private static final Logger log = Logger.getLogger(StateAutomaton.class.getName());

  /**
   * Current state.
   */
  protected Q state;

  /**
   * State transition function.
   */
  protected TransitionFunction<Q, S> transition;

  /**
   * Input symbol supplier.
   */
  protected InputSupplier<Q, S> inputSupplier;

  /**
   * Returns current state of this automaton. It is recommended always to return a <b>defensive copy</b> (due to tampering).
   * 
   * @return Current state of this automaton
   */
  public Q getState() {
    return state;
  }

  /**
   * Causes this automaton to begin execution (open and run).
   * 
   * @throws TerminatedStateAutomatonException if the automaton is terminated
   */
  public void start() throws TerminatedStateAutomatonException {
    log.finer("Initialization...");
    open();
    log.finer("Execution...");
    run();
  }

  /**
   * Represents initialization phase. All automaton properties (start state, transition function, and input supplier)
   * should be instantiated here.
   */
  protected abstract void open();

  /**
   * Represents execution phase. State is changed until the automaton enters a final state.
   * <p>
   * Listener's {@link StateAutomatonListener#automatonStarted(State) automatonStarted}, {@link StateAutomatonListener#automatonStopped(State) automatonStopped},
   * and {@link StateAutomatonListener#automatonTerminated(State, Throwable) automatonTerminated} (if an exception occurs) are invoked.
   * 
   * @throws TerminatedStateAutomatonException if the automaton is terminated
   */
  protected void run() throws TerminatedStateAutomatonException {
    notifyListeners(l -> l.automatonStarted(getState()));
    try {
      while (!finalState()) {
        changeState();
      }
      notifyListeners(l -> l.automatonStopped(getState()));
    } catch (Throwable t) {
      try {
        notifyListeners(l -> l.automatonTerminated(getState(), t));
      } catch (Throwable tNotify) {
        t.addSuppressed(tNotify);
      }
      throw t;
    }
  }

  /**
   * Is the current state final?
   * 
   * @return {@code true} iff current state of this automaton is final
   */
  protected boolean finalState() {
    return state.isFinal();
  }

  /**
   * Changes the state of the automaton.
   * <p>
   * Listener's {@link StateAutomatonListener#beforeStateChange(State) beforeStateChange},
   * {@link StateAutomatonListener#afterStateChange(State) afterStateChange}, and
   * {@link StateAutomatonListener#unsuccessfulStateChange(State, Throwable) unsuccessfulStateChange} (if an exception but
   * {@link TerminatedStateAutomatonException} occurs) are invoked.
   * <p>
   * {@link UnsuccessfulStateChangeException} is consumed,
   * {@link TerminatedStateAutomatonException} and other {@link Throwable throwables} are rethrown.
   * 
   * @throws TerminatedStateAutomatonException if the automaton is terminated
   */
  protected void changeState() throws TerminatedStateAutomatonException {
    notifyListeners(l -> l.beforeStateChange(getState()));
    try {
      state = nextState();
      log.finer(() -> String.valueOf(state));
      notifyListeners(l -> l.afterStateChange(getState()));
    } catch (TerminatedStateAutomatonException e) {
      throw e;
    } catch (UnsuccessfulStateChangeException e) {
      // Automaton remains in its current state, so:
      // 1. notification is invoked but
      // 2. exception is _not_ rethrown in order to proceed with the execution.
      // This is the main reason for defining the FiniteStateAutomaton,
      // since one might want to prevent infinite execution of the automaton.
      log.finer(() -> String.valueOf(e));
      notifyListeners(l -> l.unsuccessfulStateChange(getState(), e));
    } catch (Throwable t) {
      try {
        notifyListeners(l -> l.unsuccessfulStateChange(getState(), t));
      } catch (Throwable tNotify) {
        t.addSuppressed(tNotify);
      }
      throw t;
    }
  }

  /**
   * Asks the input supplier for the next symbol.
   * Consequently, transition function returns a new state with respect to the current state and input symbol.
   * 
   * @return next state according to the input supplier and transition function
   * 
   * @throws UnsuccessfulStateChangeException if the next state cannot be determined
   * @throws TerminatedStateAutomatonException if the automaton is terminated
   */
  protected Q nextState() throws UnsuccessfulStateChangeException, TerminatedStateAutomatonException {
    log.finer(() -> String.valueOf(state));
    S symbol = inputSupplier.apply(state);
    log.finer(() -> String.valueOf(symbol));
    return transition.apply(state, symbol);
  }

  @Override
  public String toString() {
    return String.valueOf(state);
  }
}
